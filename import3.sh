#!/bin/sh
set -x

MYSQLUSER=root 
PASSWORD=kingqueen
DB=maleaddb3

FILENAME=$1
TABLENAME=$2
MONTH=$3
YEAR=$4

mysql -u$MYSQLUSER -p$PASSWORD --database=$DB -AN --local-infile=1 -e "LOAD DATA LOCAL INFILE '$FILENAME' INTO TABLE $TABLENAME FIELDS TERMINATED BY ','  ENCLOSED BY '\"'LINES TERMINATED BY '\n' IGNORE 1 ROWS"

mysql -u$MYSQLUSER -p$PASSWORD --database=$DB -e "UPDATE $TABLENAME SET for_month='$MONTH', for_year='$YEAR' WHERE isprevious=0;"

echo '$TABLENAME'.csv Successfully Imported. 
