-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 10:54 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dmadb2`
--

-- --------------------------------------------------------

--
-- Table structure for table `axiom`
--

CREATE TABLE `axiom` (
  `axciom_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `first_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `middle_initial` varchar(100) CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dob` varchar(50) NOT NULL,
  `address_line1` varchar(800) CHARACTER SET latin1 NOT NULL,
  `address_line2` varchar(800) CHARACTER SET latin1 NOT NULL,
  `city` varchar(100) CHARACTER SET latin1 NOT NULL,
  `state` varchar(100) CHARACTER SET latin1 NOT NULL,
  `zip_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(20) CHARACTER SET latin1 NOT NULL,
  `census_tract_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `census_block_group` varchar(100) CHARACTER SET latin1 NOT NULL,
  `census_block_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `fips_county_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `income` varchar(100) CHARACTER SET latin1 NOT NULL,
  `education` varchar(500) CHARACTER SET latin1 NOT NULL,
  `married` varchar(30) CHARACTER SET latin1 NOT NULL,
  `ethnicity` varchar(50) CHARACTER SET latin1 NOT NULL,
  `personicx` varchar(200) CHARACTER SET latin1 NOT NULL,
  `personicx_cluster` varchar(200) CHARACTER SET latin1 NOT NULL,
  `for_month` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `for_year` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `clinical_data`
--

CREATE TABLE `clinical_data` (
  `clinical_data_id` int(11) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `CCMS_ID` varchar(150) DEFAULT NULL,
  `edr_member_id` int(11) DEFAULT NULL,
  `LID` text DEFAULT NULL,
  `depnum` text DEFAULT NULL,
  `medicare_id` text DEFAULT NULL,
  `hicno` text DEFAULT NULL,
  `mbi` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `first_name` text DEFAULT NULL,
  `mid_initial` text DEFAULT NULL,
  `dob` text DEFAULT NULL,
  `address1` text DEFAULT NULL,
  `address2` text DEFAULT NULL,
  `city` text DEFAULT NULL,
  `state` text DEFAULT NULL,
  `zip_code` text DEFAULT NULL,
  `inserted_date` text DEFAULT NULL,
  `for_month` text DEFAULT NULL,
  `for_year` text DEFAULT NULL,
  `is_inserted` int(11) DEFAULT NULL,
  `is_updated` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dmafile`
--

CREATE TABLE `dmafile` (
  `id` int(11) NOT NULL,
  `filetypeId` int(11) NOT NULL,
  `fileurl` varchar(250) NOT NULL,
  `userid` int(11) NOT NULL,
  `month` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `fileName` varchar(200) NOT NULL,
  `uploadedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `isUploaded` enum('Pending','Uploaded','Submitted','') NOT NULL DEFAULT 'Pending',
  `isCompletedOn` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dmafile`
--

INSERT INTO `dmafile` (`id`, `filetypeId`, `fileurl`, `userid`, `month`, `year`, `fileName`, `uploadedOn`, `isUploaded`, `isCompletedOn`) VALUES
(1, 5, 'http:\\localhost:7001\\temp\\July-2020\\Host-July-2020.csv', 2, 'July', 2020, 'Host-July-2020.csv', '2020-08-03 05:46:48', 'Uploaded', NULL),
(2, 4, 'http:\\localhost:7001\\temp\\8-2020\\EDR P65-8-2020.csv', 2, '8', 2020, 'EDR P65-8-2020.csv', '2020-09-08 10:39:47', 'Uploaded', NULL),
(3, 5, 'http:\\localhost:7001\\temp\\8-2020\\Host-8-2020.csv', 2, '8', 2020, 'Host-8-2020.csv', '2020-09-11 09:42:12', 'Uploaded', NULL),
(4, 6, 'http:\\localhost:7001\\temp\\8-2020\\National Aliance-8-2020.csv', 2, '8', 2020, 'National Aliance-8-2020.csv', '2020-09-11 09:42:38', 'Uploaded', NULL),
(5, 9, 'http:\\localhost:7001\\temp\\8-2020\\Mailings-8-2020.csv', 2, '8', 2020, 'Mailings-8-2020.csv', '2020-09-11 10:49:13', 'Uploaded', NULL),
(6, 12, 'http:\\localhost:7001\\temp\\8-2020\\SDFC Sales Source Hist-8-2020.csv', 2, '8', 2020, 'SDFC Sales Source Hist-8-2020.csv', '2020-09-17 07:19:20', 'Uploaded', NULL),
(7, 8, 'http:\\localhost:7001\\temp\\8-2020\\SDFC Leads Source Hist-8-2020.csv', 2, '8', 2020, 'SDFC Leads Source Hist-8-2020.csv', '2020-09-17 07:20:24', 'Uploaded', NULL),
(8, 1, 'http:\\localhost:7001\\temp\\8-2020\\undefined-8-2020.csv', 2, '8', 2020, 'Axciom-8-2020.csv', '2020-09-17 07:22:43', 'Uploaded', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `do_not_call`
--

CREATE TABLE `do_not_call` (
  `dnc_id` bigint(20) NOT NULL,
  `new_LID` varchar(100) NOT NULL,
  `LID` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `dep_id` varchar(100) NOT NULL,
  `member_UD` varchar(100) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `date` varchar(200) NOT NULL,
  `remove` varchar(200) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `is_inserted` int(11) NOT NULL DEFAULT 0,
  `is_updated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `edr_p65_combine_data`
--

CREATE TABLE `edr_p65_combine_data` (
  `edr_p65_combine_id` bigint(20) NOT NULL DEFAULT 0,
  `MAD_ID` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `first_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `street_address1_name` varchar(500) CHARACTER SET latin1 NOT NULL,
  `street_address2_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `city_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `state_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `zip_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `phone_number` varchar(100) CHARACTER SET latin1 NOT NULL,
  `member_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `market_segment` varchar(100) CHARACTER SET latin1 NOT NULL,
  `begin_date` date NOT NULL,
  `end_date` date NOT NULL,
  `disenrollment_reason_code` varchar(500) CHARACTER SET latin1 NOT NULL,
  `account_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `group_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `subgroup_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `consistent_member_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `member_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `medicare_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `legacy_member_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dob_date` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gender_code` varchar(20) CHARACTER SET latin1 NOT NULL,
  `member_relationship_desc` varchar(30) CHARACTER SET latin1 NOT NULL,
  `product_family_name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `legacy_benefit_package_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `for_month` varchar(50) CHARACTER SET latin1 NOT NULL,
  `for_year` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `filetype`
--

CREATE TABLE `filetype` (
  `id` int(11) NOT NULL,
  `filetype` varchar(200) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tableName` varchar(100) NOT NULL,
  `fileTypeOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `filetype`
--

INSERT INTO `filetype` (`id`, `filetype`, `createdOn`, `tableName`, `fileTypeOrder`) VALUES
(1, 'Axciom', '2020-07-31 08:40:31', 'axiom', 6),
(2, 'Clinical', '2020-07-31 08:41:24', 'clinical_data', 9),
(3, 'Do Not Copy', '2020-07-31 08:41:07', 'do_not_call', 8),
(4, 'EDR P65', '2020-07-31 08:37:28', 'edr_p65_combine_data', 1),
(5, 'Host', '2020-07-31 08:38:10', 'host_hist', 2),
(6, 'National Aliance', '2020-07-31 08:38:24', 'national_alliance_hist', 3),
(7, 'Prodcopy', '2020-07-31 08:40:47', 'prodcopy', 7),
(8, 'SDFC Leads Source Hist', '2020-07-31 08:40:14', 'sdfc_leads_source_hist', 5),
(9, 'Mailings', '2020-07-31 08:41:54', 'mailings', 11),
(10, 'Marketo', '2020-07-31 08:41:39', 'marketo', 10),
(11, 'MA Recent Deceased', '2020-07-31 08:42:14', 'ma_recent_deceased', 12),
(12, 'SDFC Sales Source Hist', '2020-07-31 08:39:13', 'sdfc_sales_source_hist', 4);

-- --------------------------------------------------------

--
-- Table structure for table `host_hist`
--

CREATE TABLE `host_hist` (
  `host_hist_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `ndw_home_plan_id` varchar(200) DEFAULT NULL,
  `consistent_member_id` varchar(200) DEFAULT NULL,
  `mmi_identifier` varchar(200) DEFAULT NULL,
  `its_subscriber_id` varchar(200) DEFAULT NULL,
  `member_date_of_birth` varchar(200) DEFAULT NULL,
  `member_gender` varchar(200) DEFAULT NULL,
  `member_prefix` varchar(200) DEFAULT NULL,
  `member_last_name` varchar(200) DEFAULT NULL,
  `member_first_name` varchar(200) DEFAULT NULL,
  `member_middle_initial` varchar(200) DEFAULT NULL,
  `member_suffix` varchar(200) DEFAULT NULL,
  `member_primary_street_address_1` varchar(500) DEFAULT NULL,
  `member_primary_street_address_2` varchar(500) DEFAULT NULL,
  `member_primary_city` varchar(200) DEFAULT NULL,
  `member_primary_state` varchar(200) DEFAULT NULL,
  `member_primary_zip_code` varchar(200) DEFAULT NULL,
  `member_primary_zip_code_4` varchar(200) DEFAULT NULL,
  `member_primary_phone_number` varchar(200) DEFAULT NULL,
  `member_primary_email_address` varchar(200) DEFAULT NULL,
  `member_secondary_street_address_1` varchar(500) DEFAULT NULL,
  `member_secondary_street_address_2` varchar(500) DEFAULT NULL,
  `member_secondary_city` varchar(200) DEFAULT NULL,
  `member_secondary_state` varchar(200) DEFAULT NULL,
  `member_secondary_zip_code` varchar(200) DEFAULT NULL,
  `member_secondary_zip_code_4` varchar(200) DEFAULT NULL,
  `reporting_account_name` varchar(200) DEFAULT NULL,
  `ndw_host_plan_id` varchar(200) DEFAULT NULL,
  `ndw_product_category_code` varchar(200) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mailings`
--

CREATE TABLE `mailings` (
  `mailing_id` bigint(20) NOT NULL,
  `date` varchar(100) DEFAULT NULL,
  `mailing_name` varchar(200) DEFAULT NULL,
  `mailing_number` varchar(200) DEFAULT NULL,
  `member_id` varchar(100) NOT NULL,
  `ccid` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address_line_1` varchar(500) DEFAULT NULL,
  `address_line_2` varchar(500) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  `mailing_hist_id` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `is_inserted` int(11) DEFAULT 0,
  `is_updated` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `maleads_integrated`
--

CREATE TABLE `maleads_integrated` (
  `integreted_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) NOT NULL,
  `MAD_STATUS` varchar(50) NOT NULL,
  `HICNO` varchar(50) NOT NULL,
  `first_name` text NOT NULL,
  `middle_initial` text NOT NULL,
  `last_name` text NOT NULL,
  `dob` varchar(50) NOT NULL,
  `address_line1` varchar(500) NOT NULL,
  `address_line2` varchar(500) NOT NULL,
  `city` varchar(80) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `census_tract_code` varchar(60) NOT NULL,
  `census_block_group` varchar(100) NOT NULL,
  `census_block_code` varchar(100) NOT NULL,
  `fips_county_code` varchar(100) NOT NULL,
  `income` varchar(100) NOT NULL,
  `education` varchar(100) NOT NULL,
  `married` varchar(30) NOT NULL,
  `ethnicity` varchar(50) NOT NULL,
  `personicx` varchar(100) NOT NULL,
  `personicx_cluster` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `group_id` varchar(50) NOT NULL,
  `subgroup_name` varchar(100) NOT NULL,
  `consistent_member_id` varchar(100) NOT NULL,
  `member_id` varchar(20) NOT NULL,
  `legacy_member_id` varchar(50) NOT NULL,
  `medicare_id` varchar(50) NOT NULL,
  `member_email` varchar(50) NOT NULL,
  `gender_code` varchar(50) NOT NULL,
  `member_relationship_desc` varchar(1000) NOT NULL,
  `product_family_name` varchar(50) NOT NULL,
  `legacy_benefit_package_id` varchar(100) NOT NULL,
  `market_segment` varchar(100) NOT NULL,
  `begin_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `disenrollment_reason_code` varchar(50) NOT NULL,
  `prospect_type` varchar(100) NOT NULL,
  `territory` varchar(100) NOT NULL,
  `lead_owner` varchar(100) NOT NULL,
  `bcbs_reference_id` varchar(50) NOT NULL,
  `lead_number` varchar(100) NOT NULL,
  `ma_effective_date` varchar(50) NOT NULL,
  `lead_source` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `lead_sub_type` varchar(100) NOT NULL,
  `created_date` varchar(50) NOT NULL,
  `current_blue_cross_id` varchar(50) NOT NULL,
  `do_not_call` varchar(50) NOT NULL,
  `unread_by_owner` varchar(50) NOT NULL,
  `converted` varchar(50) NOT NULL,
  `opportunity_date` varchar(50) NOT NULL,
  `opportunity_owner` varchar(100) NOT NULL,
  `stage` varchar(200) NOT NULL,
  `closed_lost_reason` varchar(500) NOT NULL,
  `opp_status` varchar(50) NOT NULL,
  `age_in_type` varchar(100) NOT NULL,
  `current_carrier` varchar(100) NOT NULL,
  `disenrollment_date` varchar(100) NOT NULL,
  `disenrollment_reason` varchar(500) NOT NULL,
  `sic` varchar(200) NOT NULL,
  `last_activity` varchar(50) NOT NULL,
  `last_modified` varchar(50) NOT NULL,
  `lead_id` varchar(50) NOT NULL,
  `sfdc_id` varchar(50) NOT NULL,
  `opportunity_id` varchar(50) NOT NULL,
  `lead_record_type` varchar(100) NOT NULL,
  `opportunity_record_type` varchar(200) NOT NULL,
  `any_other_contact_status` varchar(100) NOT NULL,
  `current_coverage_type` varchar(200) NOT NULL,
  `deceased` varchar(500) NOT NULL,
  `sales_date` date NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `close_date` varchar(50) NOT NULL,
  `effective_date` varchar(50) NOT NULL,
  `enrollment_channel` varchar(100) NOT NULL,
  `sep_type` varchar(100) NOT NULL,
  `enrollment_method` varchar(100) NOT NULL,
  `sales_channel` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `enrollment_source` varchar(100) NOT NULL,
  `account_id` varchar(100) NOT NULL,
  `account_record_type` varchar(100) NOT NULL,
  `for_month` int(11) NOT NULL,
  `for_year` int(11) NOT NULL,
  `axciom_id` int(11) NOT NULL,
  `edr_id` int(11) NOT NULL,
  `sfdc_leads_id` int(11) NOT NULL,
  `sfdc_sales_id` int(11) NOT NULL,
  `plan65_id` int(11) NOT NULL,
  `inserteddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `intaxiom_matched` tinyint(1) NOT NULL,
  `salesaxiom_matched` tinyint(1) NOT NULL,
  `edrsales_matched` tinyint(1) NOT NULL,
  `intedr_matched` tinyint(1) NOT NULL,
  `edraxiom_matched` tinyint(1) NOT NULL,
  `salesleads_matched` tinyint(1) NOT NULL,
  `pseudo_lead` tinyint(4) NOT NULL DEFAULT 0,
  `plan65leads_matched` int(11) NOT NULL DEFAULT 0,
  `marketo_id` int(11) NOT NULL DEFAULT 0,
  `marketo_matched` tinyint(4) NOT NULL DEFAULT 0,
  `edr_found` tinyint(4) NOT NULL,
  `host_match` tinyint(4) NOT NULL,
  `NA_match` tinyint(4) NOT NULL,
  `sales_match` tinyint(4) NOT NULL,
  `leads_match` tinyint(4) NOT NULL,
  `acxiom_match` tinyint(4) NOT NULL,
  `prodcopy_match` tinyint(4) NOT NULL,
  `dnc_match` tinyint(4) NOT NULL,
  `clinical_match` tinyint(4) NOT NULL,
  `mailings_match` tinyint(11) NOT NULL,
  `deceased_match` tinyint(11) NOT NULL,
  `isprevious_data` tinyint(4) NOT NULL,
  `MAD_Data_Source` varchar(200) NOT NULL,
  `MAD_Prospect_type` varchar(200) NOT NULL,
  `MAD_lead_type_current` varchar(250) NOT NULL,
  `MAD_Lead_type_Prior` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maleads_integrated`
--

INSERT INTO `maleads_integrated` (`integreted_id`, `MAD_ID`, `MAD_STATUS`, `HICNO`, `first_name`, `middle_initial`, `last_name`, `dob`, `address_line1`, `address_line2`, `city`, `state`, `zip_code`, `phone`, `census_tract_code`, `census_block_group`, `census_block_code`, `fips_county_code`, `income`, `education`, `married`, `ethnicity`, `personicx`, `personicx_cluster`, `account_name`, `group_id`, `subgroup_name`, `consistent_member_id`, `member_id`, `legacy_member_id`, `medicare_id`, `member_email`, `gender_code`, `member_relationship_desc`, `product_family_name`, `legacy_benefit_package_id`, `market_segment`, `begin_date`, `end_date`, `disenrollment_reason_code`, `prospect_type`, `territory`, `lead_owner`, `bcbs_reference_id`, `lead_number`, `ma_effective_date`, `lead_source`, `status`, `lead_sub_type`, `created_date`, `current_blue_cross_id`, `do_not_call`, `unread_by_owner`, `converted`, `opportunity_date`, `opportunity_owner`, `stage`, `closed_lost_reason`, `opp_status`, `age_in_type`, `current_carrier`, `disenrollment_date`, `disenrollment_reason`, `sic`, `last_activity`, `last_modified`, `lead_id`, `sfdc_id`, `opportunity_id`, `lead_record_type`, `opportunity_record_type`, `any_other_contact_status`, `current_coverage_type`, `deceased`, `sales_date`, `product_name`, `close_date`, `effective_date`, `enrollment_channel`, `sep_type`, `enrollment_method`, `sales_channel`, `country`, `enrollment_source`, `account_id`, `account_record_type`, `for_month`, `for_year`, `axciom_id`, `edr_id`, `sfdc_leads_id`, `sfdc_sales_id`, `plan65_id`, `inserteddate`, `intaxiom_matched`, `salesaxiom_matched`, `edrsales_matched`, `intedr_matched`, `edraxiom_matched`, `salesleads_matched`, `pseudo_lead`, `plan65leads_matched`, `marketo_id`, `marketo_matched`, `edr_found`, `host_match`, `NA_match`, `sales_match`, `leads_match`, `acxiom_match`, `prodcopy_match`, `dnc_match`, `clinical_match`, `mailings_match`, `deceased_match`, `isprevious_data`, `MAD_Data_Source`, `MAD_Prospect_type`, `MAD_lead_type_current`, `MAD_Lead_type_Prior`) VALUES
(1234, '1', 'No Change', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '2020-07-20 10:05:00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', ''),
(1235, '2', 'No Change', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '2020-07-20 10:05:00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', ''),
(1236, '3', 'transfer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 8, 2020, 0, 0, 0, 0, 0, '2020-09-11 13:58:38', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', ''),
(1237, '4', 'transfer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '2020-07-20 10:05:00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `marketo`
--

CREATE TABLE `marketo` (
  `marketo_id` int(11) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `date_sent` varchar(100) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `segment` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `member_id` varchar(50) NOT NULL,
  `opened` varchar(40) NOT NULL,
  `clicked` varchar(50) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `for_month` varchar(45) DEFAULT NULL,
  `for_year` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ma_recent_deceased`
--

CREATE TABLE `ma_recent_deceased` (
  `ma_recent_deceased_id` bigint(20) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `dateofbirth` varchar(200) NOT NULL,
  `age` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `discharge_date` varchar(100) NOT NULL,
  `discharge_disposition` varchar(200) NOT NULL,
  `consistent_id` varchar(200) NOT NULL,
  `LID` varchar(200) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `is_inserted` int(11) NOT NULL DEFAULT 0,
  `is_updated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `national_alliance_hist`
--

CREATE TABLE `national_alliance_hist` (
  `national_hist_alliance_id` bigint(20) NOT NULL DEFAULT 0,
  `MAD_ID` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `mbr_no` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `pat_lname` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pat_fname` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `subscr_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `subs_address1` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `subs_address2` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `subs_address3` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `subs_city` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `subs_state` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `subs_zip_cd` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pat_dob` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pat_sex` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pat_sex_rel` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `group_prfx` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `group_base` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `group_sufx` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `lob_cd` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `over_age_cd` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ce_alternate_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ttb_id_number` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pat_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `eol_indicator` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `inserted_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `for_year` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `prodcopy`
--

CREATE TABLE `prodcopy` (
  `LID` varchar(200) NOT NULL,
  `dependent_id` varchar(200) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `lastlogindate` varchar(200) NOT NULL,
  `datecreated` varchar(200) NOT NULL,
  `dateupdated` varchar(200) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `is_inserted` int(11) NOT NULL DEFAULT 0,
  `is_updated` int(11) NOT NULL DEFAULT 0,
  `prodcopy_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodcopy`
--

INSERT INTO `prodcopy` (`LID`, `dependent_id`, `email_address`, `lastlogindate`, `datecreated`, `dateupdated`, `inserted_date`, `for_month`, `for_year`, `is_inserted`, `is_updated`, `prodcopy_id`) VALUES
('', '', 'vijay@gamil.com', 'dasd', '', '\r', '2020-09-18 10:50:10', '8', '2020', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sdfc_leads_source_hist`
--

CREATE TABLE `sdfc_leads_source_hist` (
  `sfdc_leads_source_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `address1` varchar(500) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `account_id` varchar(100) DEFAULT NULL,
  `prospect_type` varchar(100) DEFAULT NULL,
  `territory` varchar(100) DEFAULT NULL,
  `lead_owner` varchar(200) DEFAULT NULL,
  `bcbs_reference_id` varchar(50) DEFAULT NULL,
  `lead_number` varchar(200) DEFAULT NULL,
  `ma_effective_date` varchar(50) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `lead_sub_type` varchar(100) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `current_blue_cross_id` varchar(50) DEFAULT NULL,
  `do_not_call` varchar(50) DEFAULT NULL,
  `unread_by_owner` varchar(50) DEFAULT NULL,
  `converted` varchar(50) DEFAULT NULL,
  `opportunity_date` date DEFAULT NULL,
  `opportunity_owner` varchar(100) DEFAULT NULL,
  `stage` varchar(200) DEFAULT NULL,
  `closed_lost_reason` varchar(500) DEFAULT NULL,
  `opp_status` varchar(50) DEFAULT NULL,
  `age_in_type` varchar(200) DEFAULT NULL,
  `current_carrier` varchar(100) DEFAULT NULL,
  `disenrollment_date` date DEFAULT NULL,
  `disenrollment_reason` varchar(500) DEFAULT NULL,
  `sic` varchar(200) DEFAULT NULL,
  `last_activity` varchar(50) DEFAULT NULL,
  `last_modified` varchar(50) DEFAULT NULL,
  `lead_id` varchar(50) DEFAULT NULL,
  `sfdc_id` varchar(50) DEFAULT NULL,
  `opportunity_id` varchar(50) DEFAULT NULL,
  `lead_record_type` varchar(100) DEFAULT NULL,
  `opportunity_record_type` varchar(200) DEFAULT NULL,
  `any_other_contact_status` varchar(100) DEFAULT NULL,
  `current_coverage_type` varchar(200) DEFAULT NULL,
  `deceased` varchar(500) DEFAULT NULL,
  `inserted_date` timestamp NULL DEFAULT current_timestamp(),
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sdfc_sales_source_hist`
--

CREATE TABLE `sdfc_sales_source_hist` (
  `sfdc_sales_source_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `birthdate` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `prospect_type` varchar(200) NOT NULL,
  `territory` varchar(100) NOT NULL,
  `bcbs_reference_id` varchar(50) NOT NULL,
  `stage` varchar(100) NOT NULL,
  `opp_status` varchar(50) NOT NULL,
  `closed_lost_reason` varchar(100) NOT NULL,
  `age_in_type` varchar(200) NOT NULL,
  `disenrollment_date` varchar(100) NOT NULL,
  `disenrollment_reason` varchar(100) NOT NULL,
  `opportunity_owner` varchar(200) NOT NULL,
  `last_activity` varchar(50) NOT NULL,
  `last_modified_date` varchar(50) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `close_date` varchar(50) NOT NULL,
  `effective_date` varchar(50) NOT NULL,
  `enrollment_channel` varchar(100) NOT NULL,
  `sep_type` varchar(100) NOT NULL,
  `medicare_id` varchar(100) NOT NULL,
  `enrollment_method` varchar(100) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `enrollment_source` varchar(200) NOT NULL,
  `lead_source` varchar(200) NOT NULL,
  `opportunity_id` varchar(50) NOT NULL,
  `account_id` varchar(50) NOT NULL,
  `opportunity_record_type` varchar(255) NOT NULL,
  `account_record_type` varchar(255) NOT NULL,
  `current_blue_cross_id` varchar(100) NOT NULL,
  `do_not_call` varchar(50) NOT NULL,
  `deceased` varchar(255) NOT NULL,
  `sales_date` varchar(50) NOT NULL,
  `sfdc_id` varchar(50) NOT NULL,
  `lead_id` varchar(50) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `for_month` varchar(50) NOT NULL,
  `for_year` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `apassword` varchar(300) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `apassword`, `reg_date`) VALUES
(2, 'sanchit', 'bhatkande', 'sanchit@gmail.com', '$2a$07$HqhZMLJmaYvVRAFn5ZMx3ufUGjCF2fz8hvFFnLQoRWDR4r/GDU8Hq', '2020-07-22 12:04:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `axiom`
--
ALTER TABLE `axiom`
  ADD PRIMARY KEY (`axciom_id`),
  ADD KEY `MAD_ID` (`MAD_ID`),
  ADD KEY `axciomid` (`axciom_id`),
  ADD KEY `formoth` (`for_month`),
  ADD KEY `foryear` (`for_year`);

--
-- Indexes for table `clinical_data`
--
ALTER TABLE `clinical_data`
  ADD PRIMARY KEY (`clinical_data_id`),
  ADD UNIQUE KEY `clinical_data_id_UNIQUE` (`clinical_data_id`),
  ADD KEY `MAD_ID` (`MAD_ID`),
  ADD KEY `consistant_id` (`CCMS_ID`);

--
-- Indexes for table `dmafile`
--
ALTER TABLE `dmafile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `do_not_call`
--
ALTER TABLE `do_not_call`
  ADD PRIMARY KEY (`dnc_id`),
  ADD UNIQUE KEY `dnc_id_UNIQUE` (`dnc_id`),
  ADD KEY `dncindex` (`dnc_id`);

--
-- Indexes for table `filetype`
--
ALTER TABLE `filetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `host_hist`
--
ALTER TABLE `host_hist`
  ADD PRIMARY KEY (`host_hist_id`),
  ADD UNIQUE KEY `host_hist_id_UNIQUE` (`host_hist_id`),
  ADD KEY `MAD_ID` (`MAD_ID`);

--
-- Indexes for table `mailings`
--
ALTER TABLE `mailings`
  ADD PRIMARY KEY (`mailing_id`),
  ADD UNIQUE KEY `mailing_id_UNIQUE` (`mailing_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `primarykey` (`mailing_id`);

--
-- Indexes for table `maleads_integrated`
--
ALTER TABLE `maleads_integrated`
  ADD PRIMARY KEY (`integreted_id`);

--
-- Indexes for table `marketo`
--
ALTER TABLE `marketo`
  ADD PRIMARY KEY (`marketo_id`);

--
-- Indexes for table `ma_recent_deceased`
--
ALTER TABLE `ma_recent_deceased`
  ADD PRIMARY KEY (`ma_recent_deceased_id`),
  ADD UNIQUE KEY `ma_recent_deceased_id_UNIQUE` (`ma_recent_deceased_id`),
  ADD KEY `consistant_id` (`consistent_id`);

--
-- Indexes for table `prodcopy`
--
ALTER TABLE `prodcopy`
  ADD PRIMARY KEY (`prodcopy_id`),
  ADD UNIQUE KEY `prodcopy_id_UNIQUE` (`prodcopy_id`),
  ADD KEY `lid` (`LID`);

--
-- Indexes for table `sdfc_leads_source_hist`
--
ALTER TABLE `sdfc_leads_source_hist`
  ADD PRIMARY KEY (`sfdc_leads_source_id`),
  ADD KEY `MAD_ID` (`MAD_ID`);

--
-- Indexes for table `sdfc_sales_source_hist`
--
ALTER TABLE `sdfc_sales_source_hist`
  ADD PRIMARY KEY (`sfdc_sales_source_id`),
  ADD KEY `MAD_ID` (`MAD_ID`),
  ADD KEY `primaryindex` (`sfdc_sales_source_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `axiom`
--
ALTER TABLE `axiom`
  MODIFY `axciom_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clinical_data`
--
ALTER TABLE `clinical_data`
  MODIFY `clinical_data_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dmafile`
--
ALTER TABLE `dmafile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `do_not_call`
--
ALTER TABLE `do_not_call`
  MODIFY `dnc_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filetype`
--
ALTER TABLE `filetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `host_hist`
--
ALTER TABLE `host_hist`
  MODIFY `host_hist_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailings`
--
ALTER TABLE `mailings`
  MODIFY `mailing_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maleads_integrated`
--
ALTER TABLE `maleads_integrated`
  MODIFY `integreted_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1238;

--
-- AUTO_INCREMENT for table `marketo`
--
ALTER TABLE `marketo`
  MODIFY `marketo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ma_recent_deceased`
--
ALTER TABLE `ma_recent_deceased`
  MODIFY `ma_recent_deceased_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prodcopy`
--
ALTER TABLE `prodcopy`
  MODIFY `prodcopy_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sdfc_leads_source_hist`
--
ALTER TABLE `sdfc_leads_source_hist`
  MODIFY `sfdc_leads_source_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sdfc_sales_source_hist`
--
ALTER TABLE `sdfc_sales_source_hist`
  MODIFY `sfdc_sales_source_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
