import java.io.*;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime; 
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.sql.Timestamp;  

import com.sforce.async.*;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

// Parameter can be sent to program which is the datetime value to be used in the query.

public class BulkQueryMain {

    public static void main(String[] args)
        throws AsyncApiException, ConnectionException, IOException, InterruptedException {	
	try
	{
        	// ******************************************************************************		
		FileReader reader=new FileReader("login.properties");
		Properties p = new Properties();     
		p.load(reader); 
		String userName = p.getProperty("username", "");
		String password = p.getProperty("password", "");
		String lastRunDate = p.getProperty("lastrundate","");
		String leadFields = p.getProperty("fields","");

		// Check if date is passed as parameter; if yes then override the lastrundate parameter with this date.
		if(args.length > 0)
		{
			//System.out.println("No arguments are expected to run this program.");
            		//System.exit(1);
			lastRunDate = args[0];
		}
		
		BulkQueryExample example = new BulkQueryExample();		
        
		String objectName = "lead";
		String query = "";
		if(leadFields.equals("")) // default query if value is not set in properties file
		{
			query = "SELECT Id,city,street,state,country,PostalCode,FirstName,LastName,Company,Phone,CreatedDate,CreatedBy.Name,DoNotCall,Email,HasOptedOutOfEmail,			LastModifiedBy.Name,Owner.Name,LeadSource,SICCode__c,Date_of_Birth__c,Prospect_type__c,Opportunity_Record_Type__c,Sales_Channel__c,Enrollment_Channel__c,Age_In_Type__c,Closed_Lost_Reason__c,Converted__c,Effective_Date__c,Close_Date__c,Product_Name__c,Disenrollment_Date__c,Disenrollment_Reason__c,Last_Activity__c,Lead_Record_Type__c,Lead_Subtype__c,MA_Effective_Date__c,Opportunity_Status__c,Opportunity_Date__c,Opportunity_Id__c,Opportunity_Owner__c,Flag__c from Lead WHERE ((CreatedDate >" + lastRunDate + ") OR (LastModifiedDate >= " + lastRunDate + " AND 		LastModifiedDate <= TODAY)) order by CreatedDate desc";
		}
		else
		{
			query = "SELECT " + leadFields + " from Lead WHERE ((CreatedDate >" + lastRunDate + ") OR (LastModifiedDate >= " + lastRunDate + 								" AND LastModifiedDate <= TODAY)) order by CreatedDate desc";
		}
        
        	String[] inputDataArray = {objectName, query, userName, password};
        	System.out.println(String.join(", ", inputDataArray));

        	if (userName.equals("") || password.equals("")) {
	    		System.out.println("Username or password missing !");
            		System.exit(1);
        	}
        	example.runSample(objectName, userName, password, query);
	}
	catch(FileNotFoundException e)
	{
		System.out.println("File exception." + e.getMessage());	
	}
	catch(IOException e)
	{
		System.out.println("I/O exception !");
	}
	catch(ConnectionException e)
	{
		System.out.println("Connection exception ! Connection could not be established with Salesforce Org.One of the reasons can be wrong username or password.");
	}
	catch(AsyncApiException e)
	{
		System.out.println("AsyncApiException exception !" + e.getMessage());
	}	
	catch(Exception e)
	{
		System.out.println("Exception :" + e.getMessage());
	}
	finally
	{}
    }
}




class BulkQueryExample {

    private PartnerConnection partnerConnection;

    /**
       * This function updates the datetime value in the property file with GMT timestamp.
    **/
	public void updatePropertyFileWithLastRunDate() 
		throws AsyncApiException, ConnectionException, IOException,InterruptedException
	{
		SimpleDateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd");
       		SimpleDateFormat gmtTimeFormat = new SimpleDateFormat("HH:mm:ss");
	
		gmtDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
       		gmtTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

       		String gmtDate = gmtDateFormat.format(new Date());
                String gmtTime = gmtTimeFormat.format(new Timestamp(System.currentTimeMillis()));
       		System.out.println("GMT time zone: " + gmtDate + "T" + gmtTime + "Z");		

		String dateLastRun = gmtDate + "T" + gmtTime + "Z" ;
		
		FileInputStream in = new FileInputStream("login.properties");
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream("login.properties");
		props.setProperty("lastrundate", dateLastRun);
		props.store(out, null);
		out.close();	
	}

    /**
     * Creates a Bulk API job and query batches for a String.
     */
    public void runSample(String sobjectType, String userName,
            String password, String query)
        throws AsyncApiException, ConnectionException, IOException, InterruptedException {
        BulkConnection connection = getBulkConnection(userName, password);
        JobInfo job = createJob(sobjectType, connection);
        BatchInfo batchInfo = createBatchesFromString(connection, job, query);
        closeJob(connection, job.getId());

        String[] queryResults = awaitCompletion(connection, job, batchInfo);
       	
        checkResults(connection, job, batchInfo, queryResults);
	System.out.println("Update the property file");
        updatePropertyFileWithLastRunDate();  
	System.out.println("Update complete");
        partnerConnection.logout();
    }
	

    /**
     * Gets the results of the operation and checks for errors.
     */
    private void checkResults(BulkConnection connection, JobInfo job,
            BatchInfo batchInfo, String[] queryResults)
        throws AsyncApiException, IOException 
   {
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	String downloadFilename = "LeadsFromSF_" + sdf1.format(timestamp) + ".csv";
	System.out.println("Filename with timestamp:" + downloadFilename);

	FileWriter myWriter = new FileWriter(downloadFilename);
        for (String queryResult : queryResults) {
            CSVReader rdr =
                new CSVReader(connection.getQueryResultStream(job.getId(), batchInfo.getId(), queryResult));
            List<String> resultHeader = rdr.nextRecord();
            int resultCols = resultHeader.size();
		
            for (int i = 0; i < resultCols; i++) 
	    {
                	System.out.print(resultHeader.get(i));
			myWriter.write(resultHeader.get(i));
			if(i <= (resultCols-2))
			{
				System.out.print(",");                    	   
				myWriter.write(",");
			}
            }
            myWriter.write("\n");
            System.out.println();

            List<String> row;
	    String dataVal;
	    
            while ((row = rdr.nextRecord()) != null) 
            {
                for (int i = 0; i < resultCols; i++) 
		{  
			dataVal = "";  	
			try
			{
				dataVal = row.get(i);
			}
			catch(Exception e)
			{
				//System.out.println("Exception due to blank string 1 :" + e.getMessage());
			}
			try
			{			
				System.out.print(dataVal);	
			}
			catch(Exception e)
			{
				//System.out.println("Exception at system.out.print 2 :" + e.getMessage());
			}
			try
			{		
				myWriter.write(dataVal);	
			}
			catch(Exception e)
			{
				//System.out.println("Exception at file write 3 :" + e.getMessage());
			}
			if(i <= (resultCols-2))
			{
				System.out.print(",");                    	   
				myWriter.write(",");
			}
						
                }
                System.out.println();
		myWriter.write("\n");
            }
		myWriter.close();
        }
    }



    private void closeJob(BulkConnection connection, String jobId)
        throws AsyncApiException {
        JobInfo job = new JobInfo();
        job.setId(jobId);
        job.setState(JobStateEnum.Closed);
        connection.updateJob(job);
    }


    /**
     * Wait for a job to complete by polling the Bulk API.
     * 
     * @param connection
     *            BulkConnection used to check results.
     * @param jobInfo
     *            The job awaiting completion.
     * @param batchInfo
     *            Batche for this job.
     * @throws AsyncApiException
     */
    private String[] awaitCompletion(BulkConnection connection, JobInfo jobInfo,
            BatchInfo batchInfo)
        throws AsyncApiException, InterruptedException {
        for(int i=0; i<10000; i++) {
            Thread.sleep(i==0 ? 30 * 1000 : 30 * 1000); //30 sec
            batchInfo = connection.getBatchInfo(jobInfo.getId(),
                    batchInfo.getId());
            if (batchInfo.getState() == BatchStateEnum.Completed) {
                QueryResultList list =
                    connection.getQueryResultList(jobInfo.getId(),
                        batchInfo.getId());
                String[] queryResults = list.getResult();
                //System.out.println("queryResults: " + String.join(", ", queryResults));
                return queryResults;
            } else if (batchInfo.getState() == BatchStateEnum.Failed) {
                System.out.println("-------------- failed ----------"
                    + batchInfo);
                break;
            } else {
                System.out.println("-------------- waiting ----------"
                    + batchInfo);
            }
        }
        return new String[]{};
    }



    /**
     * Create a new job using the Bulk API.
     * 
     * @param sobjectType
     *            The object type being loaded, such as "Account"
     * @param connection
     *            BulkConnection used to create the new job.
     * @return The JobInfo for the new job.
     * @throws AsyncApiException
     */
    private JobInfo createJob(String sobjectType, BulkConnection connection)
        throws AsyncApiException {
        JobInfo job = new JobInfo();
        job.setObject(sobjectType);
        job.setOperation(OperationEnum.query);
        job.setContentType(ContentType.CSV);
        job = connection.createJob(job);
        System.out.println(job);
        return job;
    }



    /**
     * Create the BulkConnection used to call Bulk API operations.
     */
    private BulkConnection getBulkConnection(String userName, String password)
        throws ConnectionException, AsyncApiException {
        ConnectorConfig partnerConfig = new ConnectorConfig();
        partnerConfig.setUsername(userName);
        partnerConfig.setPassword(password);
        partnerConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/51.0");
        // Creating the connection automatically handles login and stores
        // the session in partnerConfig
        partnerConnection = new PartnerConnection(partnerConfig);
        // When PartnerConnection is instantiated, a login is implicitly
        // executed and, if successful,
        // a valid session is stored in the ConnectorConfig instance.
        // Use this key to initialize a BulkConnection:
        ConnectorConfig config = new ConnectorConfig();
        config.setSessionId(partnerConfig.getSessionId());
        // The endpoint for the Bulk API service is the same as for the normal
        // SOAP uri until the /Soap/ part. From here it's '/async/versionNumber'
        String soapEndpoint = partnerConfig.getServiceEndpoint();
        String apiVersion = "51.0";
        String restEndpoint = soapEndpoint.substring(0, soapEndpoint.indexOf("Soap/"))
            + "async/" + apiVersion;
        config.setRestEndpoint(restEndpoint);
        // This should only be false when doing debugging.
        config.setCompression(true);
        // Set this to true to see HTTP requests and responses on stdout
        config.setTraceMessage(false);
        BulkConnection connection = new BulkConnection(config);
        return connection;
    }



    /**
     * Create and upload batches using a CSV file.
     * The file into the appropriate size batch files.
     * 
     * @param connection
     *            Connection to use for creating batches
     * @param jobInfo
     *            Job associated with new batches
     * @param query
     *            SOQL query.
     */
    private BatchInfo createBatchesFromString(BulkConnection connection,
            JobInfo jobInfo, String query)
        throws IOException, AsyncApiException {
        InputStream is = new ByteArrayInputStream(query.getBytes());
	//System.out.println("In createBatchesFromString:" + is);
        return connection.createBatchFromStream(jobInfo, is);
    }    
}


   
