-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: maleaddb3
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `axiom`
--

DROP TABLE IF EXISTS `axiom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axiom` (
  `first_name` varchar(100) DEFAULT NULL,
  `middle_initial` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `address_line1` varchar(800) DEFAULT NULL,
  `address_line2` varchar(800) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `census_tract_code` varchar(100) DEFAULT NULL,
  `census_block_group` varchar(100) DEFAULT NULL,
  `census_block_code` varchar(100) DEFAULT NULL,
  `fips_county_code` varchar(100) DEFAULT NULL,
  `income` varchar(100) DEFAULT NULL,
  `education` varchar(500) DEFAULT NULL,
  `married` varchar(30) DEFAULT NULL,
  `ethnicity` varchar(50) DEFAULT NULL,
  `personicx` varchar(200) DEFAULT NULL,
  `personicx_cluster` varchar(200) DEFAULT NULL,
  `axciom_id` bigint(20) NOT NULL,
  `MAD_ID` varchar(200) DEFAULT NULL,
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `isprevious` int(11) DEFAULT 0,
  `ORI_MAD_ID` varchar(245) DEFAULT NULL,
  `is_mad_updated` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `axiom`
--

LOCK TABLES `axiom` WRITE;
/*!40000 ALTER TABLE `axiom` DISABLE KEYS */;
INSERT INTO `axiom` VALUES ('Ross','Paul','Delacourt','19970101','94 Meadow Vale Point','9 Tennyson Pass','Richmond','Virginia','23242','8045253791','23242','1','19','1','778.55','Kitami Institute of Technology','Married','Q','10B Rural-Metro Mix','33 Urban Diversity',1,'RosDel521823242','','',2,'RosDel521823242',0),('Abramo','Antony','Jurges','19220508','1397 Hermina Way','4 Independence Junction','Austin','Texas','78783','5127527124','78783','1','3','7','46935.3','Kawamura Gakuen Woman\'s University','Single','W','17M Bargain Hunters','63 Staying Home',2,'AbrJur543878783','','',2,'AbrJur543878783',0),('Casandra','Ara','Gooday','19981108','51 Bunker Hill Way','1440 Sherman Drive','Pittsburgh','Pennsylvania','15266','8405062','15266','4','90','8','23997.03','St. Mary\'s University of Minnesota','married','E','14B Diverging Paths','25 Clubs and Causes',3,'CasGoo515715266','','',2,'CasGoo515715266',0),('Netta','Skippie','Farlowe','20060325','92 Oneill Lane','33458 Sundown Point','Dallas','Texas','75241','4694360413','75241','4','79','9','12158.2','Indian Institute of Technology, Kanpur','married','R','21S Leisure Seekers','06 Casual Comfort',4,'NetFar473675241','','',2,'NetFar473675241',0),('Jeniffer','Else','Assaf','19990204','74851 Londonderry Drive','43456 Trailsway Junction','Phoenix','Arizona','85010','6021592443','85010','1','60','8','92042.84','Fukuoka Institute of Technology','Single','T','20S Community Minded','17 Firmly Established',5,'JenAss618285010','','',2,'JenAss618285010',0),('Rosana','Allianora','Hollington','19450208','1102 Lillian Hill','41 Hanson Lane','Fresno','California','93715','5599007602','93715','4','71','2','89624.03','Universidad de la Fraternidad de Agrupaciones Santo Tomas de Aquino (FASTA)','Single','Y','07X Career Oriented','09 Busy Schedules',6,'RosHol494993715','','',2,'RosHol494993715',0),('Marvin','Jon','Aitcheson','20160930','073 Susan Street','0 Golf Center','Birmingham','Alabama','35263','2057143978','35263','3','45','1','47928.15','Baker College of Flint','Married','U','12B Comfortable Households','05 Active & Involved',7,'MarAit477635263','','',2,'MarAit477635263',0),('Whitney','Cherish','Ropert','19220308','4 Meadow Vale Terrace','36 Petterle Pass','New York City','New York','10170','6461176064','10170','4','66','9','71319.74','St. Petersburg State Conservatory','Single','I','19M Solid Prestige','02 Established Elite',8,'WhiRop540110170','','',2,'WhiRop540110170',0),('Shanan','Lay','Thandi','19210923','95452 Anderson Road','47752 Magdeline Place','Winston Salem','North Carolina','27150','3361502318','27150','1','40','8','16543.62','Firat (Euphrates) University','married','O','15M Top Wealth','46 Rural & Active',9,'ShaTha521727150','','',2,'ShaTha521727150',0),('Titus','Mathe','Goold','19890219','6 Eastlawn Crossing','1152 Dapin Point','Oklahoma City','Oklahoma','73104','4055519796','73104','2','71','9','8670.21','Naval Postgraduate School','married','P','03X Settling Down','01 Summit Estates',10,'TitGoo486573104','','',2,'TitGoo486573104',0),('Norri','Valma','Langrick','19320529','4828 Oxford Road','7378 Talisman Parkway','Evansville','Indiana','47705','8125251647','47705','1','46','4','65145.66','Universidade do Estado de Santa Catarina','Single','A','11B Affluent Households','13 Work & Play',11,'NorLan522047705','','',2,'NorLan522047705',0),('Roland','Amelie','Pestricke','19580613','26 Dovetail Park','047 Sheridan Plaza','Modesto','California','95397','2091247153','95397','3','34','1','64259.26','Chuo University','Single','D','13B Working Households','49 Home & Garden',12,'RolPes524895397','','',2,'RolPes524895397',0),('Aguie','Isabeau','Carp','19611106','71 Elka Drive','68396 Dakota Lane','Washington','District of Columbia','20551','2022079154','20551','1','90','3','79112.75','Shanghai Jiaotong University','Married','S','04X Social Connectors','03 Corporate Connected',13,'AguCar398420551','','',2,'AguCar398420551',0),('Cecilla','Kristen','Sones','19271117','8111 Park Meadow Point','31993 Hallows Parkway','Phoenix','Arizona','85030','6027793917','85030','1','18','5','86484.65','National Institute of Technology Kurukshetra','Single','F','02Y Taking Hold','36 Persistent & Productive',14,'CecSon556385030','','',2,'CecSon556385030',0),('Patrick','Norrie','Bozward','19530219','3426 Fairview Way','9 Thackeray Center','Lexington','Kentucky','40505','8596686240','40505','3','21','4','76462.15','North East University Bangladesh','married','G','18M Thrifty and Active','08 Solid Surroundings',15,'PatBoz525040505','','',2,'PatBoz525040505',0),('Kyrstin','Nevins','Morat','19600207','556 Parkside Pass','42 Vernon Pass','Chicago','Illinois','60674','3127305620','60674','2','58','5','28414.58','Islamic Azad University, Fars Science & Research Branch','married','H','16M Living Well','07 Active Lifestyles',16,'KyrMor467760674','','',2,'KyrMor467760674',0),('Nevile','Bibi','Camfield','19360115','81 Briar Crest Circle','969 Pleasure Center','Lake Worth','Florida','33467','5613201455','33467','1','84','3','76467.91','Universidad Nacional de San Luis','Single','J','05X Busy Households','28 Community Pillars',17,'NevCam562933467','','',2,'NevCam562933467',0),('Shelton','Annis','Tebbitt','19240306','74658 Farragut Circle','82206 Sheridan Lane','Waco','Texas','76796','2543975749','76796','2','54','2','11751.05','Physical Education Academy \"Eugeniusz Piasecki\" in Poznan','Single','K','08X Large Households','04 Top Professionals',18,'SheTeb549876796','','',2,'SheTeb549876796',0),('Rubina','Olive','MacAnulty','19500111','87 Lotheville Center','70030 Little Fleur Trail','Carol Stream','Illinois','60158','3095567160','60158','4','79','10','9125.65','University of Oulu','Married','L','01Y Starting Out','53 Metro Strivers',19,'RubMac607960158','','',2,'RubMac607960158',0);
/*!40000 ALTER TABLE `axiom` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-11 12:00:35
