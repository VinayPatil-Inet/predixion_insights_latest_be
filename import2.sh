#!/bin/sh
set -x

sh ./import3.sh uploads//axiom.csv axiom $1 $2
sh ./import3.sh uploads//do_not_call.csv do_not_call $1 $2
sh ./import3.sh uploads//edr_p65_combine_data.csv edr_p65_combine_data $1 $2
sh ./import3.sh uploads//marketo.csv marketo $1 $2
sh ./import3.sh uploads//sdfc_leads_source_hist.csv sdfc_leads_source_hist $1 $2
sh ./import3.sh uploads//sdfc_sales_source_hist.csv sdfc_sales_source_hist $1 $2
sh ./import3.sh uploads//website.csv prodcopy $1 $2


