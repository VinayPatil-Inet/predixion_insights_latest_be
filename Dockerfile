FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm cache verify

RUN npm install

COPY . .

EXPOSE 7001

CMD ["npm", "run", "prod"]
