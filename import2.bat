@echo off
SET MONTH=%1
SET YEAR=%2

CALL import3 uploads\\axiom.csv axiom %1 %2
CALL import3 uploads\\do_not_call.csv do_not_call %1 %2
CALL import3 uploads\\edr_p65_sales_source_hist.csv edr_p65_sales_source_hist %1 %2
CALL import3 uploads\\marketo.csv marketo %1 %2
CALL import3 uploads\\sdfc_leads_source_hist.csv sdfc_leads_source_hist %1 %2
CALL import3 uploads\\sdfc_sales_source_hist.csv sdfc_sales_source_hist %1 %2
CALL import3 uploads\\website.csv prodcopy %1 %2
