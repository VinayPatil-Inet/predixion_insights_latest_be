
var express = require("express");
var app = express();
var routes = require("./routes").routes;
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var config = require("./config");
var path = require("path");
var cors = require("cors");
var compression = require("compression");
var helmet = require("helmet");
  
require('./fn/endecrypt');
app.use(compression());
// app.use(cors({origin:'*','methods': 'GET,HEAD,PUT,PATCH,POST,DELETE'}));
app.use(helmet()); 
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// app.use('/uploads', express.static(path.join(__dirname, 'public')))

// app.use('/uploads', express.static('public'))
app.use('/uploads', express.static(__dirname + '/uploads'));
// app.use(express.static(__dirname + '/uploads'));
 
app.use(express.json({limit: "100mb"}));
app.use(
  express.urlencoded({
    limit: "10000mb",
    extended: true,
    parameterLimit: 500000
  })
);
app.use(cors());
app.options('*', cors());
app.use("/api/rest", routes); // Set the default version to latest.

exports.app = app;
