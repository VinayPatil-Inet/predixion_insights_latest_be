var app = require('./app.js').app;
var http = require('http');
let server = http.Server(app); 
var config = require("./config");
var multer = require('multer');
var logger = require('./logger.js')(__filename);

// var GridFsStorage = require('multer-gridfs-storage');
// var Grid = require('gridfs-stream');
var port = 7001;
var gfs;
var dbName, fileUpload;
dbName = config.con.db
logger.info("DMA WS Running On " + process.env.NODE_ENV + " Mode..!");
logger.info("Connection Name : ----- " + process.env.NODE_ENV);

 
server.listen(port, function (err, open) {
  if (err) {
    logger.error('Port Already in use : -----' + port);
  } else {
    logger.info('Server Listening On Port : ----- ' + port);
  }
}); 


module.exports = server; 





