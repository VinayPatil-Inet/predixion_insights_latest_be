// console.log("Inside configuration file....", process.env.NODE_ENV);
if (process.env.NODE_ENV === 'prod') {
  module.exports = {
    'con': {
      'conName': 'prod',
      'host': 'localhost', // ex: 'host': '129.533.332.95',
      'user': 'root',
      'password': 'kingqueen',
      'port': 3306, 
      'db': 'maleaddb3',
      'email': 'admin@test.in',
      'dmaFeUrl': 'http://localhost:4200/',
      'dmaBeUrl': 'http://localhost:7001/',
      'importExecB': 'batchImport.bat',
      'importExecS': 'batchImport.sh',
      'temp': '../temp',
      'internalSource': '../healthchain'
    }
  };
} else if (process.env.NODE_ENV === 'test') {
  module.exports = {
    'con': {
      'conName': 'test',
      'host': 'localhost', // ex: 'host': '129.533.332.95',
      'user': 'root',
      'password': 'kingqueen',
      'port': 3306, 
      'db': 'maleaddb3',
      'email': 'admin@test.in',
      'dmaFeUrl': 'http://localhost:4200/',
      'dmaBeUrl': 'http://localhost:7001/',
      'importExecB': 'batchImport.bat',
      'importExecS': 'batchImport.sh',
      'temp': '../temp',
      'internalSource': '../healthchain'
    }
  };
} else if (process.env.NODE_ENV === 'local') {
  module.exports = {
    'con': {
      'conName': 'local',
      'host': 'localhost',
      'user': 'root',
      'password': 'kingqueen',
      'port': 3306, 
      'db': 'maleaddb3',
      'email': 'admin@test.in',
      'dmaFeUrl': 'http://localhost:4200/',
      'dmaBeUrl': 'http://localhost:7001/',
      'importExecB': 'batchImport.bat',
      'importExecS': 'batchImport.sh',
      'temp': '../temp',
      'internalSource': '../healthchain'
    },
  };
}
else {
  module.exports = {
    'con': {
      'conName': 'local',
      'host': 'localhost',
      'user': 'root',
      'password': 'kingqueen',
      'port': 3306, 
      'db': 'maleaddb3',
      'email': 'admin@test.in',
      'dmaFeUrl': 'http://localhost:4200/',
      'dmaBeUrl': 'http://localhost:7001/',
      'importExecB': 'batchImport.bat',
      'importExecS': 'batchImport.sh',
      'temp': '../temp',
      'internalSource': '../healthchain'
    },
  };
}
