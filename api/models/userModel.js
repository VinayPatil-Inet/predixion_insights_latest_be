const sql = require("../db.js");
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(7);
var JWT = require('../fn/JWT');

// constructor
const User = function (user) {
  this.email = user.email;
  this.firstname = user.firstname;
  this.lastname = user.lastname;
  // this.apassword = user.apassword;
  this.apassword = bcrypt.hashSync(user.apassword, salt);
};

User.create = (newCustomer, result) => {
  console.log("inside create model", newCustomer); 
  sql.query("SELECT * FROM user WHERE email = ?", [newCustomer.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length > 0) {
      result({
        kind: "already_exist"
      }, null);
    } else { 
      sql.query("INSERT INTO user SET ?", newCustomer, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
        result(null, {
          id: res.insertId,
          ...newCustomer
        });
      }); 
    }
  }); 
};


User.signIn = (userdata, result) => {
  console.log("Inside Sign in Function.", userdata.apassword);
  sql.query("SELECT * FROM user WHERE email = ?", [userdata.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log(res.length, "response.....");
    if (res.length > 0) {
      console.log("found user: ", res[0].apassword, userdata.apassword);
      var userpass = userdata.apassword;
      var depassword = res[0].apassword;
      if (bcrypt.compare(userpass, depassword)) {
        var user = {
          id: res[0].id,
          firstname: res[0].firstname,
          lastname: res[0].lastname,
          email: res[0].email,
          logo: res[0].logo
        }
        var studentObject = {
          user: user,
          token: JWT.issue(user)
        }
        result(null, studentObject);
        return;
      } else {
        result({
          kind: "not_found"
        }, null);
      }
    }
    result({
      kind: "not_found"
    }, null);
  });
};

User.findById = (customerId, result) => {
  sql.query(`SELECT * FROM user WHERE id = ${customerId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found User with the id
    result({
      kind: "not_found"
    }, null);
  });
};

User.getAll = result => {
  sql.query("SELECT * FROM user", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("user: ", res);
    result(null, res);
  });
};

User.updateById = (id, user, result) => {
  sql.query(
    "UPDATE user SET email = ?, name = ?, active = ? WHERE id = ?",
    [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found User with the id
        result({
          kind: "not_found"
        }, null);
        return;
      }

      console.log("updated user: ", {
        id: id,
        ...user
      });
      result(null, {
        id: id,
        ...user
      });
    }
  );
};

User.remove = (id, result) => {
  sql.query("DELETE FROM user WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found User with the id
      result({
        kind: "not_found"
      }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

User.removeAll = result => {
  sql.query("DELETE FROM user", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} user`);
    result(null, res);
  });
};

module.exports = User;