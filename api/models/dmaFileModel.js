const sql = require('../db.js');
const config = require('../config.js')
const child_process = require('child_process');
const process = require('process')
const fs = require('fs')
const path = require('path');
const { DateTime } = require('mssql');
const shBeginLine = "#!/bin/sh";




// // constructor
const DmaFile = function (dmaFile) {
    console.log('dma file-----------------------------------------', dmaFile);
    this.userid = dmaFile.userid;
    this.fileurl = dmaFile.fileurl;
    this.filetypeId = dmaFile.filetypeId;
    this.year = dmaFile.year;
    this.month = dmaFile.month;
    this.fileName = dmaFile.fileName;
    this.isUploaded = dmaFile.isUploaded;
    this.execFile = dmaFile.execFile;
    this.tableName = dmaFile.tableName;
    this.folderPath = dmaFile.folderPath;
};

DmaFile.runProcess = (processData, result) => {

    console.log("inside DmaFile.runProcess for ", processData);

    try {
        //Run the import Batch prepared earlier during import
        processFiles(processData);
    }
    catch (err) {
        result(err, null);
        return;
    }
};

processFiles = function (processData) {
    var oSys = process.platform;
    var basePath = __dirname;

    console.log("inside processFiles : ", processData);

    sql.query("SELECT * FROM dmafile WHERE userid = ? AND  month = ? AND year = ? AND isUploaded = ?", [processData.userId, processData.month, processData.year, 'imported'], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length > 0) {

            sql.query("delete FROM SP_Progress WHERE formonth = ? AND foryear = ?", [processData.month, processData.year], (err, res) => {
                console.log("Deleted from SP_Progress.............................................");
            });
            console.log(res.length, "Imported.............................................");
            var fileData = {
                isUploaded: "imported",
                userid: processData.userId,
                month: processData.month,
                year: processData.year
            };
            DmaFile.ExecuteLeadProcessor(fileData);
        } else {
            console.log("Uploaded.............................................", processData);



            var processCommand = GetProcessorQuery(processData);
            var execFileName = config.con.importExecS;
            var cleanUpCommand = processCommand + "\n\n" + "rm *";

            if (oSys == "win32") {
                execFileName = config.con.importExecB;
                cleanUpCommand = processCommand + "\n\n" + "del /q *.*";
            }

            const processPath = config.con.temp;

            let fullPath = path.join(__dirname, processPath, processData.userId, processData.year, processData.month);
            let fullExecFilePath = path.join(fullPath, execFileName).replace(/\\/g, '/');
            //console.log("basePath ||| fullPath", basePath, "||| ", fullPath) ;


            try {
                if (fs.existsSync(fullExecFilePath)) {

                    if (oSys == "linux") {
                        console.log("adding exec perms to :", fullExecFilePath);
                        fs.chmodSync(fullExecFilePath, 0o755);
                    };

                    //console.log("changing to exec path before exec") ;
                    //process.chdir(fullPath);

                    //add clean up code before running the batch file.
                    fs.appendFile(fullExecFilePath, cleanUpCommand, err => {
                        if (err) {
                            console.error(err)
                            return
                        }
                        console.log(fullExecFilePath + " appended with clean up code.");

                        const outFile = fs.openSync('./outFile.log', 'a');
                        const errFile = fs.openSync('./errFile.log', 'a');

                        //Now run the batch/shell file to import the data
                        const ls = child_process.spawn(fullExecFilePath, [], {
                            detached: true,
                            stdio: ['ignore', outFile, errFile],
                            cwd: fullPath,
                            windowsHide: true
                        });

                        ls.unref();

                        //ls.stdout.on('data', (data) => {
                        //    console.log(data);
                        //});
                        //ls.on('close', (code) => {

                        //console.log("exit code :" + code," changing to base path after exec");
                        //process.chdir(basePath) ;
                        //});
                        var fileData = {
                            isUploaded: "imported",
                            userid: processData.userId,
                            month: processData.month,
                            year: processData.year
                        };

                        DmaFile.updateById(fileData);

                        //DmaFile.ExecuteLeadProcessor(fileData) ;



                    });

                }
                else {
                    var msg = "No files found to process.";
                    console.log(msg);
                    //throw new Error(msg);
                }
            }
            catch (err) {
                throw err;
            }
        }
    });
};

GetProcessorQuery = function (data) {

    var process = "call All_SP_Processing('{{month}}','{{year}}',false)"
    var procStr = `mysql -u{{user}} -p{{password}} --database={{dbname}} -e "${process}"`
    var fullStr = replaceTextWithinFile(procStr, data);

    return fullStr;

}

DmaFile.ExecuteLeadProcessor = function (fileData, result) {

    console.log("Calling SQL Process now");
    sql.query("call All_SP_Processing(?,?,?)", [fileData.month, fileData.year, false], function (err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;

        } else {
            //result(null, res) ;
            console.log("results:", res);
        }

    });

};

DmaFile.create = (fileData, result) => {

    console.log("inside create dma file model", fileData);

    if (fileData.fileName == 'Salesforce Leads') {
        var dmaFile = {
            userid: fileData.userid,
            fileurl: fileData.fileurl,
            filetypeId: fileData.filetypeId,
            year: fileData.year,
            month: fileData.month,
            fileName: fileData.fileName,
            isUploaded: 'Uploaded',
            isSalesForce: 1,
            isSalesForcePull: 1,
            SalesforcePulledDate: DateTime()

        };
    }
    else {
        var dmaFile = {
            userid: fileData.userid,
            fileurl: fileData.fileurl,
            filetypeId: fileData.filetypeId,
            year: fileData.year,
            month: fileData.month,
            fileName: fileData.fileName,
            isUploaded: 'Uploaded'

        };

    }



    sql.query("SELECT * FROM dmafile WHERE userid = ? AND filetypeId = ? AND month = ? AND year = ?", [dmaFile.userid, dmaFile.filetypeId, dmaFile.month, dmaFile.year], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length > 0) {
            console.log(res.length, "Update.............................................");
            sql.query("UPDATE dmafile SET ? WHERE userid = ? AND filetypeId = ? AND month = ? AND year = ?", [dmaFile, dmaFile.userid, dmaFile.filetypeId, dmaFile.month, dmaFile.year], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                console.log("Update response............................................");
                result(null, res);
            });
        } else {
            console.log("Insert.............................................", dmaFile);
            sql.query("INSERT INTO dmafile SET ?", dmaFile, (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                result(null, res);
            });
        }
    });
    //Create the batch files required to import the data
    if (fileData.execFile) {
        DmaFile.createExecFile(fileData);
    }
    //-------
};

DmaFile.insertleaddata = function (leaddata, result) {
    console.log("insertleaddata.............................................", leaddata);
    // sql.query("INSERT INTO saleaforce_pushed_data SET ?", leaddata, (err, res) => {
    var resquery = "INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) VALUES ?";
    sql.query(resquery, [leaddata], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
}

DmaFile.createExecFile = function (fileData, result) {

    var oSys = process.platform;
    var execFileName = config.con.importExecS;
    var importTemplate = "../../importTemplateL.txt";
    var beginLine = shBeginLine + "\n";

    if (oSys == "win32") {
        execFileName = config.con.importExecB;
        importTemplate = "../../importTemplateB.txt";
        beginLine = "@echo off" + "\n";
    }


    console.log("inside createExecFile with :", fileData);
    let templatePath = path.join(__dirname, importTemplate).replace(/\\/g, '/');

    //let fullExecFilePath = path.join(fileData.execPath, execFileName).replace(/\\/g,'/');
    let fullExecFilePath = path.join(__dirname, fileData.folderPath, execFileName).replace(/\\/g, '/');

    try {
        if (fs.existsSync(fullExecFilePath)) {

            fs.readFile(templatePath, 'utf8', (err, data) => {
                if (err) {
                    console.error(err)
                    return;
                }
                console.log(data);
                console.log("Starting replacement");

                var content = replaceTextWithinFile(data, fileData);

                fs.appendFile(fullExecFilePath, content, err => {
                    if (err) {
                        console.error(err)
                        return
                    }
                    console.log(fullExecFilePath + " appended")
                })

            });

        } else {
            fs.readFile(templatePath, 'utf8', (err, data) => {
                if (err) {
                    console.error(err)
                    return;
                }
                console.log(data);
                console.log("Starting replacement");
                var content = beginLine + replaceTextWithinFile(data, fileData);

                fs.writeFile(fullExecFilePath, content, err => {
                    if (err) {
                        console.error(err)
                        return
                    }
                    console.log(fullExecFilePath + " Created")
                })

            });

        }

    }
    catch (err) {
        console.error(err)
    }


};

replaceTextWithinFile = function (input, fileData, result) {

    let db_user = config.con.user;
    let db_password = config.con.password;
    let db_name = config.con.db;

    var replacements = new Map([
        ["{{user}}", db_user],
        ["{{password}}", db_password],
        ["{{dbname}}", db_name],
        ["{{filename}}", fileData.fileName],
        ["{{tablename}}", fileData.tableName],
        ["{{month}}", fileData.month],
        ["{{year}}", fileData.year]
    ]);

    var result = input.replace(/{{[^}]+}}/g, function (m) {
        return replacements.get(m) || m;
    });

    return result;

}

DmaFile.getFileTypeList = (req, result) => {

    //sql.query(`SELECT id, filetype, tablename, Operation FROM filetype where id not in (SELECT filetypeId from dmafile where userid=? and month=? and year=?);`, [userId, month, year] ,(err, res) => {
    sql.query(`SELECT id, filetype, tablename, Operation FROM filetype;`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length > 0) {
            console.log("FileTypes found ", res[0]);
            result(null, res);
            return;
        }

    });
};

DmaFile.findById = (fileJson, result) => {
    console.log("getSalesforceFileList", fileJson.userId);
    sql.query(`SELECT * FROM dmafile where userid=${fileJson.userId} and isSalesForce=1 and fileName='Salesforce Leads'`, (err, res) => {
        // sql.query(`SELECT * FROM dmafile where userId=${fileJson.userId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length > 0) {
            console.log("FileTypes found=== ", res);
            result(null, res);
            return;
        }
    });
};

DmaFile.findDmaListById = (fileJson, result) => {
    console.log("Get DMA status : ", fileJson);

    sql.query(`select ft.filetype, fileName, UploadedOn, IsUploaded as Status from dmafile dm inner join filetype ft on dm.filetypeId = ft.id where userid=${fileJson.userId} and month='${fileJson.fileMonth}' and year='${fileJson.fileYear}' order by uploadedOn desc`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length > 0) {
            console.log("found dma file: ", res);
            result(null, res);
            return;
        }
        // not found User with the id
        result({
            kind: "not_found"
        }, null);
    });
};

DmaFile.updateById = (fileData, result) => {
    sql.query(
        "UPDATE dmafile SET isUploaded = ? WHERE userid = ? AND month = ? AND year = ? ",
        [fileData.isUploaded, fileData.userid, fileData.month, fileData.year],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found User with the id
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            //result(null, res);
        }
    );
};

DmaFile.updateleadsById = (fileData, result) => {
    sql.query(
        "UPDATE saleaforce_pushed_data SET status = ? WHERE user_id = ? AND for_month = ? AND for_year = ? ",
        [fileData.status, fileData.user_id, fileData.for_month, fileData.for_year],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found User with the id
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            //result(null, res);
        }
    );
};
module.exports = DmaFile;