const NodeRSA = require('node-rsa');
const key = new NodeRSA({b: 512});

exports.encryptValue = function (value, callBack) {
    return callBack(null, key.encrypt(value, 'base64', 'utf-8'))
};

exports.decryptValue = function (encryptedValue, callBack) {
    // return key.decrypt(encryptedValue, 'utf8');
    return callBack(null, key.decrypt(encryptedValue, 'utf8'))
    // return callBack(null, key.decrypt(encryptedValue, 'base64'))
    // msg = key.decrypt(msg, 'base64', 'utf8', ursa.RSA_PKCS1_PADDING);
}