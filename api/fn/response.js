exports.success = function (data, statusCode, msg) {
  var resp = {
    // 'data': {
    //   'data': data
    // },
    'data': data,
    'success': true,
    'statusCode': statusCode || 200,
    'message': msg || "success"
  };
  return resp;
};

exports.failure = function (statusCode, msg) {
  var resp = {
    'success': false,
    'statusCode': statusCode || 500,
    'message': msg || "error"
  };
  return resp;
};