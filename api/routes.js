var routes = require('express')();
var authfilter = require('./fn/filter').authFilter;
var multer = require('multer');
var upload = multer({
  'dest': 'storage/csv/'
});
var logger = require('./logger.js')(__filename);

// Import Controller.
var userController = require('./controllers/userController');
var fileController = require('./controllers/fileStorageController');




routes.get('/ping', function (req, res) {
  res.send('pong');
});

//These are api's used for debugging purposes. kept Commented
routes.post('/private/app/v1/debugFileUpload', fileController.debugFileUpload);
routes.post('/private/app/v1/debugProcess', fileController.debugProcess);
routes.post('/private/app/v1/debugUserAPI', userController.debugUserAPI);

//User APIs
routes.post('/private/dma/v1/user/create', userController.create);
routes.post('/private/dma/v1/user/signIn', userController.signIn);

// Routes.
routes.post('/private/app/v1/exportSFDC', fileController.ExportSFDC);

routes.post('/private/app/v1/process', fileController.Process);
routes.post('/private/app/v1/importSFDC', fileController.ImportSFDC);
routes.post('/private/dma/v1/user/importHealthChain', fileController.ImportHealthChain);
routes.post('/private/dma/v1/user/fileUpload', fileController.fileUpload);

routes.post('/private/dma/v1/user/files/type', fileController.getFileTypeList);
routes.post('/private/dma/v1/user/files/SalesforceList', fileController.getSalesforceFileList);
routes.post('/private/dma/v1/user/files/SalesforcePushedDownload', fileController.getSalesforcePushedData);
routes.post('/private/dma/v1/user/files/SalesforcePulledDownload', fileController.getSalesforcePulledData);

//Get filter data
routes.post('/private/dma/v1/user/files/city', fileController.getCityData);
routes.post('/private/dma/v1/user/files/state', fileController.getStateData);
routes.post('/private/dma/v1/user/files/zipcode', fileController.getZipcodeData);
routes.post('/private/dma/v1/user/files/income', fileController.getIncomeData);
routes.post('/private/dma/v1/user/files/age', fileController.getAgeData);
//select All Row
routes.post('/private/dma/v1/user/files/selectAllRow', fileController.selectAllRow);

routes.post('/private/dma/v1/user/dma/list', fileController.findDmaList);
routes.post('/private/dma/v1/user/movefile', fileController.moveFiles);
routes.post('/private/dma/v1/table/get', fileController.getTableField);
routes.post('/private/dma/v1/table/integrated', fileController.getTableIntegrated);
routes.post('/private/dma/v1/table/pagination', fileController.getDataTablePagination);
routes.post('/private/dma/v1/insertsaleforcedata', fileController.InsertSalesforcePushedData);
routes.post('/private/dma/v1/updatesaleforcepusheddata', fileController.UpdateSalesforcePushedData);
routes.post('/private/dma/v1/updateprediction', fileController.UpdatePrediction);
routes.post('/private/dma/v1/updatepredictionscore', fileController.updatePredictionScore);

routes.post('/private/dma/v1/memberretention', fileController.memberretention);
routes.post('/private/dma/v1/getmemberdetails', fileController.getmemberdetails);



routes.post('/private/dma/v1/user/files/SPProgressStatus', fileController.getSPProgressStatus);

exports.routes = routes;

logger.info('Routes loaded successfully...');
