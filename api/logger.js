'user strict';
const {
  createLogger,
  transports,
  format
} = require("winston");
const fs = require('fs');
const path = require('path');
require('winston-daily-rotate-file');
const level = process.env.LOG_LEVEL || 'debug';
const logDir = 'logs';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}
const dailyRotateFileTransport = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-logs.log`,
  datePattern: 'YYYY-MM-DD'
});

const logger = caller => {
  return createLogger({
    level: level,
    format: format.combine(
      format.label({
        label: path.basename(caller)
      }),
      format.colorize(),
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      format.printf(info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)
    ),
    transports: [
      new transports.Console({
        level: level,
        format: format.combine(
          format.colorize(),
          format.printf(
            info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
          )
        )
      }),
      dailyRotateFileTransport
    ]
  });
}
module.exports = logger;