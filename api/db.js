const mysql = require("mysql");
const dbConfig = require("./config");
var logger = require('./logger.js')(__filename);

// MySQL Connection.
var connection = mysql.createConnection({
    connectionLimit: 50,
    host: dbConfig.con.host,
    user: dbConfig.con.user,
    password: dbConfig.con.password,
    database: dbConfig.con.db,
    port: dbConfig.con.port,
    connectTimeout: 100000,
    acquireTimeout: 100000,
    debug: false
});

connection.connect(function (err, connected) {
    if (err) {
        console.log("My Sql DB Connect Error *** ", err);
    } else {
        console.log("My Sql DB Connected To : -----  ", dbConfig.con.db);
    }
});

module.exports = connection;