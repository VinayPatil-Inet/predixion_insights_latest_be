const multer = require('multer');
const multerS3 = require('multer-s3');
var logger = require("../logger.js")(__filename);
var fs = require('fs');
var os = require('os');
const process = require('process');
const path = require("path");
const spawn = require('child_process').spawn;

const axios = require('axios');
const qs = require('qs');

const moveFile = require('move-file');

var config = require("../config");
const sql = require("../db.js");

const DmaFile = require("../models/dmaFileModel");
const e = require('express');
const Json2csvParser = require("json2csv").Parser;
filename = "";
filepath = "";


//API - /private/app/v1/debugProcess
exports.debugProcess = function (req, res, callBack) {

    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;

    var processData = {
        userId: userId,
        year: fileYear,
        month: fileMonth
    };

    DmaFile.ExecuteLeadProcessor(processData, (err, data) => {
        if (err)
            return res.status(500).send({
                message: err.message || "Error with Processing the files."
            });

        res.status(200).send({
            message: "Files imported Successfully!"
        });
    });

};

//API - /private/app/v1/process 
exports.Process = function (req, res, callBack) {
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;

    var processData = {
        userId: userId,
        year: fileYear,
        month: fileMonth
    };
    // Save User in the database
    DmaFile.runProcess(processData, (err, data) => {
        if (err)
            return res.status(500).send({
                message: err.message || "Error with Processing the files."
            });

        if (data) {
            console.log(data);
            res.status(200).send({
                message: "Processing done..."
            });
        }
    });
    console.log("message:", "Files imported Successfully!");
}

//API - /private/app/v1/importSFDC
exports.ImportSFDC = function (req, res, callBack) {
    if (os.platform().startsWith('linux')) {
        console.log("Import SFDC leads data...req :", req);

        const ls = spawn('./import.sh', []);
        ls.stdout.on('data', (data) => {
            var str = data.toString().replace(/</g, '&lt;');
            console.log("Import of data from SFDC started, waiting to complete...", str);
        });
        ls.on('close', (code) => {
            console.log("Import process successfull with exit code", code);
            let fileMonth = '' + `${req.body.fileMonth}`;
            let fileYear = '' + `${req.body.fileYear}`;
            let userId = '' + `${req.body.userId}`;
            let fileTypeId = '' + `${req.body.fileTypeId}`;
            let fileName = "Salesforce Leads";
            let tableName = "leads";
            const dmaFile = new DmaFile({
                userid: userId,
                fileurl: fileName,
                filetypeId: fileTypeId,
                year: fileYear,
                month: fileMonth,
                fileName: fileName,
                tableName: tableName,
                isUploaded: 'Imported',
                execFile: false
            });
            console.log("------------ before DMA create -----------------: ", dmaFile);
            // Save User in the database
            DmaFile.create(dmaFile, (err, data) => {
                if (err)
                    return res.status(500).send({
                        message: err.message || "Some error occurred while creating the User."
                    });
                else return res.status(201).json(data);
            });
        });
    } else if (os.platform().startsWith('win')) {
        console.log("not implemented on Windows yet");
    }
};

//API - /private/app/v1/exportSFDC
exports.ExportSFDC = function (req, res, callBack) {
    console.log("ExportSFDC=== ", req.body);
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;
    // let fileTypeId = '' + `${req.body.fileTypeId}`;
    let fileName = "Salesforce Leads";


    if (os.platform().startsWith('linux')) {
        // sql.query("SELECT * FROM `lead`", function (error, data, fields) {
        sql.query("SELECT salesforce_id as Id,city as City,address_line1 as Street,state as State,country as Country,zip_code as PostalCode,first_name as FirstName,last_name as LastName,product_family_name as Company,phone as Phone, do_not_call as DoNotCall,member_email as Email,lead_source as LeadSource,sic as SICCode_c,STR_TO_DATE(dob, '%m-%d-%Y') as Date_of_Birthc,prospect_type as Prospect_typec,opportunity_record_type as Opportunity_Record_Typec,sales_channel as Sales_Channelc,enrollment_channel as Enrollment_Channelc,agein_type_rel as Age_In_Typec,closed_lost_reason as Closed_Lost_Reasonc,converted as Convertedc, STR_TO_DATE(effective_date, '%m-%d-%Y') as Effective_Datec, STR_TO_DATE(close_date, '%m-%d-%Y') as Close_Datec,product_name as Product_Namec, STR_TO_DATE(disenrollment_date, '%m-%d-%Y') as Disenrollment_Datec,disenrollment_reason as Disenrollment_Reasonc,last_activity as Last_Activityc,lead_record_type as Lead_Record_Typec,lead_sub_type as Lead_Subtypec,STR_TO_DATE(ma_effective_date, '%m-%d-%Y') as MA_Effective_Datec,opp_status as Opportunity_Statusc, STR_TO_DATE(opportunity_date, '%d-%m-%Y') as Opportunity_Datec , opportunity_id as Opportunity_Idc,opportunity_owner as Opportunity_Ownerc,GIC_Mbr_Flag as Flag_c FROM maleaddb3.salesforce_leads;", function (error, data, fields) {
            if (error) throw error;
            const jsonData = JSON.parse(JSON.stringify(data));
            console.log("jsonData", jsonData);
            const json2csvParser = new Json2csvParser({
                header: true
            });
            const csv = json2csvParser.parse(jsonData);
            fs.writeFile("/root/leadpoc/upload/lead.csv", csv, function (error) {
                if (error) throw error;
                const spawn = require('child_process').spawn;
                const ls = spawn('./export.sh', []);
                ls.stdout.on('data', (data) => {
                    var test = data.toString().replace(/</g, '&lt;');
                    console.log(test, "Success...");
                });
                //  ls.stderr.on('data', (data) => {
                //    console.log(data, "Error");
                //  });

                ls.on('close', (code) => {
                    console.log(code, "UPDATE salesforce push");
                    sql.query("UPDATE dmafile SET isSalesForcePush=1 WHERE userid = ? AND fileName = ? AND month = ? AND year = ?", [userId, fileName, fileMonth, fileYear], (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        console.log("Update response............................................");
                        result(null, res);
                    });

                    res.status(200).send({
                        message: "export Successfully!"
                    });
                });
                //  console.log("lead.csv successfully Created !");
            });
        });
    } else if (os.platform().startsWith('win')) {
        console.log("not implemented on Windows yet");
    }

};


var storage = multer.diskStorage({

    destination: function (req, file, callback) {
        var dir = `../../api/temp/${req.body.userId}/${req.body.fileYear}/${req.body.fileMonth}`;
        var fullDir = path.join(__dirname, dir).replace(/\\/g, '/');
        console.log("creating folder here", fullDir);
        if (!fs.existsSync(fullDir)) {
            fs.mkdirSync(fullDir, {
                recursive: true
            });
        }
        callback(null, fullDir);
    },

    filename: function (req, file, callback) {
        var filename = `${file.originalname}`;
        this.filename = `${file.originalname}`;
        console.log("inside multer: filename=", filename);
        callback(null, filename);
    }
});

var upload = multer({
    storage: storage
}).array('upload', 12);


exports.getFileTypeList = (req, res) => {
    DmaFile.getFileTypeList(req, (err, data) => {
        if (err) {

            res.status(500).send({
                message: "Error retrieving upload file list",
            });

        } else res.json(data);
    });

};

//API - /private/dma/v1/user/importHealthChain
exports.ImportHealthChain = function (req, res) {
    console.log("inside interalUpload");
    console.log("request body : ", req.body);

    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let fileUserId = '' + `${req.body.userId}`;
    let fileTypeId = '' + `${req.body.fileTypeId}`;
    let tableName = '' + `${req.body.tableName}`;
    let fileName = '' + tableName + ".csv";

    try {
        let sourceFolderPath = config.con.internalSource;
        let sourceFilePath = path.join(__dirname, sourceFolderPath, fileName).replace(/\\/g, '/');
        console.log("sourceFilePath : ", sourceFilePath);

        let destPath = config.con.temp;
        let destFolderPath = path.join(__dirname, destPath, fileUserId, fileYear, fileMonth).replace(/\\/g, '/');
        let destFilePath = path.join(destFolderPath, fileName).replace(/\\/g, '/');

        console.log("destFilePath : ", destFilePath);

        if (!fs.existsSync(sourceFilePath)) {
            var message = "Internal file not found for copy. Please check.";
            console.log("source file exists? :", message);
            res.status(500).send({
                message: message
            });

            return res;
        }

        if (!fs.existsSync(destFolderPath)) {
            fs.mkdirSync(destFolderPath, {
                recursive: true
            });
        };

        console.log("copy process...")
        fs.copyFile(sourceFilePath, destFilePath, function (err) {
            if (err) {
                res.status(500).send({
                    message: "Error during internal file copy..."
                });
            } else {
                console.log('Successfully copied from internal to user folder!');

                req.body.fileName = fileName;
                req.body.fileTypeId = fileTypeId;
                req.body.tableName = tableName;
                res = createDMARecord(req, res);
            }
        });

    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: "Error: File processing failed!"
        });
    }
    return res;
}

createDMARecord = function (req, res) {

    let fileName = '' + `${req.body.fileName}`;
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let fileUserId = '' + `${req.body.userId}`;
    let fileTypeId = '' + `${req.body.fileTypeId}`;
    let tableName = '' + `${req.body.tableName}`;
    let initPath = config.con.temp;

    console.log("createDMARecord--------");

    let folderPath = path.join(initPath, fileUserId, fileYear, fileMonth).replace(/\\/g, '/');
    let filePath = path.join(folderPath, fileName).replace(/\\/g, '/');

    console.log("initPath : ", initPath);
    console.log("folderPath = " + folderPath);
    console.log("filePath = " + filePath);

    console.log("filetypeId : ", fileTypeId);
    console.log("tablename : ", tableName);

    const dmaFile = new DmaFile({
        userid: fileUserId,
        fileurl: filePath,
        filetypeId: fileTypeId,
        year: fileYear,
        month: fileMonth,
        fileName: fileName,
        tableName: tableName,
        isUploaded: 'Uploaded',
        folderPath: folderPath,
        execFile: true
    });

    console.log("------------ before DMA create -----------------: ", dmaFile);

    // Save User in the database
    DmaFile.create(dmaFile, (err, data) => {
        if (err)
            return res.status(500).send({
                message: err.message || "Some error occurred while creating the User."
            });
        else return res.status(201).json(data);
    });
    //AddExec will come here

}

//API - /private/dma/v1/user/fileUpload
exports.fileUpload = function (req, res, callBack) {
    console.log("###1");
    upload(req, res, function (err) {
        console.log(req.body, "file type....................................................");
        if (err) {
            res.status(500).send({
                message: "Some error occurred while uploading File."
            });
        } else {
            // File url save to database.
            if (!req.body) {
                res.status(400).send({
                    message: "Content can not be empty!"
                });
            }

            res = createDMARecord(req, res);

        }

        return res;

    });
};

//This is more of a testing function, for this route /private/app/v1/addExec, which is disabled by default
//Guru Belthur - 20211011
exports.debugFileUpload = function (req, res, callback) {

    try {
        let fileName = '' + `${req.body.fileName}`;
        let fileMonth = '' + `${req.body.fileMonth}`;
        let fileYear = '' + `${req.body.fileYear}`;
        let fileUserId = '' + `${req.body.fileUserId}`;
        let fileTypeId = '' + `${req.body.fileTypeId}`;
        let TableName = '' + `${req.body.fileTable}`;
        let initPath = config.con.temp;
        let folderPath = path.join(initPath, fileUserId, fileYear, fileMonth).replace(/\\/g, '/');
        let filePath = path.join(folderPath, fileName).replace(/\\/g, '/');


        fileData = {
            fileName: fileName,
            month: fileMonth,
            year: fileYear,
            userId: fileUserId,
            fileTypeId: fileTypeId,
            filepath: filePath,
            folderPath: folderPath,
            tableName: TableName
        };

        var result = DmaFile.createExecFile(fileData);

    } catch (err) {
        // Handle failure
        console.error(err);
        res.status(500).send({
            message: "Error: File processing failed!"
        });
    };

    return res.status(200).send({
        message: result
    });
};

exports.getFileType = (req, res) => {
    DmaFile.findById(req.params.customerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found file with id ${req.params.customerId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving file with id " + req.params.customerId
                });
            }
        } else res.json(data);
    });
};

exports.getSalesforceFileList = (req, res) => {
    console.log("getSalesforceFileList", req.body);
    DmaFile.findById(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found data with id ${req.body.userId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving data with id " + req.body.userId
                });
            }
        } else res.json(data);
    });
};


exports.memberretention = (req, res) => {
    console.log("Inside ..", req.body);

    var fromDate = '' + req.body.fromDate;
    var toDate = '' + req.body.toDate;

    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;
    var age = '' + req.body.age;
    var income = '' + req.body.income;
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;
    let fileName = "Salesforce Leads";
    let isCheckedLeadsAll = `${req.body.isCheckedLeadsAll}`;
    var integratedLead = '' + req.body.integratedLead;
    var filterquery = '';

    var assignee = '' + req.body.assignee;
    var disenrollment = '' + req.body.disenrollment;
    var cummunicationReference = '' + req.body.cummunicationReference;
    var outreachStage = '' + req.body.outreachStage;
    var memberPrioritization = '' + req.body.memberPrioritization;

    // if (isCheckedLeadsAll === true) {
    //     console.log("true..................................................................................", req.body);
    //     filterquery = ` and integreted_id not in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.fileMonth} and for_year = ${req.body.fileYear} and status='pushed')`;
    // }

    if (req.body.assignee.length > 0) {
        var assigneeArr = assignee.split(',');
        console.log(assigneeArr, "stateArr")
        const assignedToFinal = assigneeArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and assignedTo in (${assignedToFinal})`;
    }

    if (req.body.disenrollment.length > 0) {
        var disenrollmentArr = disenrollment.split(',');
        console.log(disenrollmentArr, "disenrollmentArr")
        const disenrollmentFinal = disenrollmentArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and predictedRiskofDisenrollment in (${disenrollmentFinal})`;
    }

    if (req.body.cummunicationReference.length > 0) {
        var cummunicationReferenceArr = cummunicationReference.split(',');
        console.log(cummunicationReferenceArr, "cummunicationReferenceArr")
        const cummunicationReferenceFinal = cummunicationReferenceArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and predictedcommunicationpreference in (${cummunicationReferenceFinal})`;
    }

    if (req.body.outreachStage.length > 0) {
        var outreachStageArr = outreachStage.split(',');
        console.log(outreachStageArr, "outreachStageArr")
        const outreachStageFinal = outreachStageArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and outreachStage in (${outreachStageFinal})`;
    }

    if (req.body.memberPrioritization.length > 0) {
        var memberPrioritizationArr = memberPrioritization.split(',');
        console.log(memberPrioritizationArr, "memberPrioritizationArr")
        const memberPrioritizationFinal = memberPrioritizationArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and predictedmemberprioritization in (${memberPrioritizationFinal})`;
    }


    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery =  filterquery + ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zipcode in (${zipcodeFinal})`;
    }
    if (req.body.income.length > 0) {
        var incomeArr = income.split(',');
        console.log(incomeArr, "incomeArr")
        const incomeFinal = incomeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and income in (${incomeFinal})`;
    }
    if (req.body.age.length > 0) {
        filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) BETWEEN  ${req.body.age.min} AND ${req.body.age.max}`;
    }
    if (req.body.integratedLead.length > 0) {
        var integratedLeadArr = integratedLead.split(',');
        console.log(integratedLeadArr, "integratedLeadArr")
        const integratedLeadFinal = integratedLeadArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and integreted_id in (${integratedLeadFinal})`;
    }

    var disquery = `SELECT * FROM predixion_insights WHERE retention_date between '${req.body.fromDate}' and '${req.body.toDate}' `

 
    //var disquery = `INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) SELECT integreted_id,for_month,for_year,1 as user_id FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear} `

    if (filterquery != '') {
        disquery = disquery + filterquery;
    }

    console.log(disquery, "disquery")
    sql.query(disquery, '', (err, result) => {
        if (err) {
            console.log("error: ", err);
            return;
        }
        console.log("result: ", result);
        return res.status(200).json(result);
    });

};

exports.findDmaList = (req, res) => {
    console.log(req.body, "file paramiters......................vvvvvvvv.................");
    DmaFile.findDmaListById(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `File Not Found`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving file with id " + req.params.customerId
                });
            }
        } else res.json(data);
        // } else res.send(data); 
    });
};

exports.moveFiles = (req, res) => {
    console.log("move file function...........");
    const files = fs.readdirSync(`api/temp/${req.body.month}-${req.body.year}/`)

    var dir = `api/uploads/${req.body.month}-${req.body.year}`;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    // if (files.length < 8) {
    //   return res.status(400).send({
    //     message: "Info : Please Upload All Files..!"
    //   });
    // }

    var result = [];
    for (const file of files) {

        console.log(file);
        (async () => {
            try {
                await moveFile(`api/temp/${req.body.month}-${req.body.year}/${file}`, `api/uploads/${req.body.month}-${req.body.year}/${file}`);
                result.push(file);
                console.log('The files count', result.length, files.length);
                if (result.length === files.length) {
                    console.log("All Files Moved Successfully.....");
                    // return res.status(201).send({
                    //   message: "Success: All Files Are Uploaded Successfully..!"  
                    // });

                    //----
                    const dmaFile = new DmaFile({
                        userid: req.body.userId,
                        year: req.body.year,
                        month: req.body.month,
                        isUploaded: 'Submitted'
                    });
                    // Save User in the database
                    DmaFile.updateById(dmaFile, (err, data) => {
                        if (err)
                            return res.status(500).send({
                                message: err.message || "Some error occurred while creating the User."
                            });
                        else return res.status(201).send({
                            message: "Success: All Files Are Uploaded Successfully..!"
                        });
                    });
                    //----
                }
            } catch (err) {
                // Handle failure
                console.error(err);
                res.status(500).send({
                    message: "Error: File Upload Failed.!"
                });
            }
        })();
    }
}



// Get Integrated Table Details..
exports.getTableIntegrated = (req, res) => {
    console.log("get Integrated Table request ...........", req.body);
    // sql.query(`select * FRom maleads_integrated WHERE for_month = ? AND for_year = ?` , [req.body.fileMonth, req.body.fileYear], (err, data) => {
    sql.query(`select * FRom maleads_integrated`, (err, data) => {
        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);
    });
};

function buildTable(data) {
    var dataArray = [];
    data.forEach(function (tableData) {
        console.log(tableData.COLUMN_NAME);
        var dataObject = {
            'COLUMN_NAME': tableData.COLUMN_NAME,
            'ORDINAL_POSITION': tableData.ORDINAL_POSITION,
        }

        if (tableData.COLUMN_NAME !== "for_month" && tableData.COLUMN_NAME !== "for_year") {
            dataArray.push(dataObject);
        }


        // dataArray.push(dataObject);
    });
    return dataArray;
}


// Get datatable Pagination API.
exports.getDataTablePagination = (req, res) => {
    console.log("Get Data Table API ...........", req.body);
    var length = req.body.length; //lenght
    var start = req.body.start
    var limit = start + ',' + length;
    var column = req.body.order[0].column;
    if (req.body.order[0].column === 0) {
        column = 1
    }
    var state = '' + req.body.dateObject.state;
    var city = '' + req.body.dateObject.city;
    var zipcode = '' + req.body.dateObject.zipcode;
    var age = '' + req.body.dateObject.age;
    var income = '' + req.body.dateObject.income;
    var score = req.body.dateObject.score;

    var isChecked = req.body.dateObject.isChecked;

    var dir = req.body.order[0].dir;
    var search = req.body.search.value

    var filterquery = '';
    // sql.query(`SELECT count(*) as numRows FROM  maleads_integrated where (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and 
    //  for_month  = ${req.body.dateObject.fileMonth} and for_year = ${req.body.dateObject.fileYear}`, function (err, rows, fields) {

    var sqlquery = `SELECT count(*) as numRows FROM  maleads_integrated where (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and 
    for_month  = ${req.body.dateObject.fileMonth} and for_year = ${req.body.dateObject.fileYear} `;

    if (isChecked === true) {
        console.log(isChecked, "true.......................................")
        filterquery = ` and salesforce_id is null and integreted_id not in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.dateObject.fileMonth} and for_year = ${req.body.dateObject.fileYear} and status='pushed')`;
    }

    if (req.body.dateObject.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.dateObject.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.dateObject.zipcode.length > 0) {


        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }
    if (req.body.dateObject.income.length > 0) {

        console.log(income, "income")




        var incomeArr = income.split(/,(?=(?:(?:[^"]*"){2})*[^"]*$)/);

        // res = str.split(/,(?=(?:(?:[^"]"){2})[^"]*$)/);
        // var incomeArr = incomeres.split(',');
        console.log(incomeArr, "incomeArr")
        const incomeFinal = incomeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and income in (${incomeFinal})`;
    }

    if (req.body.dateObject.score) {
        console.log(score.length, "Array111111111111111111111111111111111111111111............................");
        var scoreArray = [];
        for (i = 0; i < score.length; i++) {

            if (score[i].ischecked) {
                var ids = score[i].id;
                var scoreArr = ids.split('t');
                scoreArray.push(scoreArr[0]);
                scoreArray.push(scoreArr[1]);
            }

        }
        console.log(scoreArray, "Array............................")

        if (scoreArray.length > 0) {
            var scoreMax = Math.max.apply(Math, scoreArray);
            var scoreMin = Math.min.apply(Math, scoreArray);
            //     var scores = score.split('t');
            console.log(scoreMax, scoreMin, "scores....")
            filterquery = filterquery + ` and prediction_score BETWEEN  ${scoreMin} AND  ${scoreMax} `;
        }
    }

    if (req.body.dateObject.age) {
        // var ageArr = age.split(',');
        // console.log(ageArr, "ageArr")
        // const ageFinal = ageArr.map(c => `'${c}'`).join(', ');
        // filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) in (${ageFinal})`;
        filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) BETWEEN  ${req.body.dateObject.age.min} AND ${req.body.dateObject.age.max}`;
    }
    // if (req.body.dateObject.age.length > 0) {
    //     var ageArr = age.split(',');
    //     console.log(ageArr, "ageArr")
    //     const ageFinal = ageArr.map(c => `'${c}'`).join(', ');
    //     filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) in (${ageFinal})`;
    // }
    if (filterquery != '') {
        sqlquery = sqlquery + filterquery;
    }

    sql.query(sqlquery, function (err, rows, fields) {
        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        } else {
            var numRows = rows[0].numRows;
            var numPages = Math.ceil(numRows / length);
            // sql.query(`SELECT * FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.dateObject.fileMonth} and for_year = ${req.body.dateObject.fileYear}  ORDER BY ${column} ${dir} LIMIT ${limit}`, function (err, rows, fields) {

            var disquery = `SELECT *, TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) as age, STR_TO_DATE(dob,'%m-%d-%Y') AS dobview  FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.dateObject.fileMonth} and for_year = ${req.body.dateObject.fileYear}`

            if (filterquery != '') {
                disquery = disquery + filterquery;
            }


            console.log("disquery select............ ", disquery);

            sql.query(disquery + `  ORDER BY ${column} ${dir} LIMIT ${limit}`, function (err, rows, fields) {
                if (err) {
                    console.log("error: ", err);
                    return res.status(500).send({
                        message: err
                    });
                } else {
                    var data = {
                        data: rows,
                        draw: req.body.draw,
                        recordsFiltered: numRows,
                        recordsTotal: numRows
                    };
                    //  console.log(data)
                    return res.status(201).json(data);
                }
            });
        }
    });
};

exports.getSalesforcePushedData = (req, res) => {
    console.log("Get Data Table API getSalesforcePushedData ...........", req.body);

    sql.query(`SELECT MAD_ID,city,address_line1,state,country,zip_code,first_name,last_name,product_family_name,phone,created_date,middle_initial,do_not_call,member_email,last_modified,lead_owner,lead_source,sic,dob,prospect_type,opportunity_record_type,sales_channel,enrollment_channel,agein_type_rel,closed_lost_reason,converted,effective_date,close_date,product_name,disenrollment_date,disenrollment_reason,last_activity,lead_record_type,lead_sub_type,ma_effective_date,opp_status,opportunity_date,opportunity_id,opportunity_owner,GIC_Mbr_Flag FROM  maleads_integrated INNER JOIN saleaforce_pushed_data on saleaforce_pushed_data.integreted_id= maleads_integrated.integreted_id WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and maleads_integrated.for_month  = ${req.body.fileMonth} and maleads_integrated.for_year = ${req.body.fileYear}`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};

exports.getmemberdetails = (req, res) => {
    console.log("Get getmemberdetails", req.body);

    sql.query(`SELECT * FROM maleaddb3.predixion_insights WHERE integrated_id  = ${req.body.id}`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No data Found !"
            });
        }
        return res.status(200).json(data);

    });
};


exports.getCityData = (req, res) => {
    console.log("Get API getCityData...........", req.body);

    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;

    var filterquery = '';

    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }

    var disquery = `SELECT @rownum := @rownum + 1 AS item_id, table1.city as item_text FROM (SELECT DISTINCT city
          FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}`

    if (filterquery != '') {
        disquery = disquery + filterquery;
    }
    console.log("city resulted query ", disquery + `) table1 JOIN (SELECT @rownum := 0) r`)
    sql.query(disquery + `) table1
  JOIN (SELECT @rownum := 0) r`, '', (err, data) => {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(404).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};
exports.getStateData = (req, res) => {
    console.log("Get API getStateData...........", req.body);

    //   sql.query(`SELECT @rownum := @rownum + 1 AS item_id, table1.state as item_text FROM   (SELECT DISTINCT state FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}) table1
    //   JOIN (SELECT @rownum := 0) r`, function (err, data, fields) {

    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;

    var filterquery = '';

    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }

    var disquery = `SELECT @rownum := @rownum + 1 AS item_id, table1.state as item_text FROM   (SELECT DISTINCT state FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}`

    if (filterquery != '') {
        disquery = disquery + filterquery;
    }
    console.log("State resulted query ", disquery + `) table1 JOIN (SELECT @rownum := 0) r`)

    sql.query(disquery + `) table1 JOIN (SELECT @rownum := 0) r`, '', (err, data) => {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(404).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};


exports.getZipcodeData = (req, res) => {
    console.log("Get API getZipcodeData...........", req.body);

    // sql.query(`SELECT @rownum := @rownum + 1 AS item_id, table1.zip_code as item_text FROM   (SELECT DISTINCT zip_code FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}) table1
    // JOIN (SELECT @rownum := 0) r`, function (err, data, fields) {
    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;

    var filterquery = '';

    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }
    var disquery = `SELECT @rownum := @rownum + 1 AS item_id, table1.zip_code as item_text FROM (SELECT DISTINCT zip_code FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}`

    if (filterquery != '') {
        disquery = disquery + filterquery;
    }
    console.log("zip resulted query ", disquery + `) table1 JOIN (SELECT @rownum := 0) r`)

    sql.query(disquery + `) table1 JOIN (SELECT @rownum := 0) r`, '', (err, data) => {
        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(404).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);
    });
};


exports.getIncomeData = (req, res) => {
    console.log("Get API getIncomeData...........", req.body);

    sql.query(`SELECT @rownum := @rownum + 1 AS item_id,
  table1.income as item_text
FROM   (SELECT DISTINCT income
   FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}) table1
  JOIN (SELECT @rownum := 0) r`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(404).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};
exports.getAgeData = (req, res) => {
    console.log("Get API getAgeData...........", req.body);


    sql.query(`SELECT @rownum := @rownum + 1 AS item_id,
  table1.age as item_text
FROM   (SELECT DISTINCT TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) as age
  FROM   maleads_integrated where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}) table1
 JOIN (SELECT @rownum := 0) r`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(404).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};

exports.selectAllRow = (req, res) => {
    console.log("Get API Select All API..", req.body);
    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;
    var age = '' + req.body.age;
    var income = '' + req.body.income;
    var isChecked = req.body.isChecked;
    var isCheckedLeadsAll = req.body.isCheckedLeadsAll;
    var score = req.body.score;
    var filterquery = '';
    var sqlquery = `SELECT count(*) as numRows FROM  maleads_integrated where (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and 
    for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear} `;
    if (isChecked === true) {
        console.log(isChecked, "true.......................................")
        filterquery = ` and integreted_id not in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.fileMonth} and for_year = ${req.body.fileYear} and status='pushed')`;
    }
    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }
    if (req.body.income.length > 0) {
        var incomeArr = income.split(',');
        console.log(incomeArr, "incomeArr")
        const incomeFinal = incomeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and income in (${incomeFinal})`;
    }

    if (req.body.score) {
        console.log(score.length, "Array111111111111111111111111111111111111111111............................");
        var scoreArray = [];
        for (i = 0; i < score.length; i++) {

            if (score[i].ischecked) {
                var ids = score[i].id;
                var scoreArr = ids.split('t');
                scoreArray.push(scoreArr[0]);
                scoreArray.push(scoreArr[1]);
            }

        }
        console.log(scoreArray, "Array............................")

        if (scoreArray.length > 0) {
            var scoreMax = Math.max.apply(Math, scoreArray);
            var scoreMin = Math.min.apply(Math, scoreArray);
            //     var scores = score.split('t');
            console.log(scoreMax, scoreMin, "scores....")
            filterquery = filterquery + ` and prediction_score BETWEEN  ${scoreMin} AND  ${scoreMax} `;
        }
    }
    // if (req.body.age.length > 0) {
    //     var ageArr = age.split(',');
    //     console.log(ageArr, "ageArr")
    //     const ageFinal = ageArr.map(c => `'${c}'`).join(', ');
    //     filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) in (${ageFinal})`;
    // }

    if (req.body.age) {
        filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) BETWEEN  ${req.body.age.min} AND ${req.body.age.max}`;
    }
    if (filterquery != '') {
        sqlquery = sqlquery + filterquery;
    }
    console.log("sqlquery==== ", sqlquery);

    sql.query(sqlquery, function (err, rows, fields) {
        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        } else {
            var disquery = `SELECT integreted_id FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}`
            if (filterquery != '') {
                disquery = disquery + filterquery;
            }
            sql.query(disquery, function (err, rows, fields) {
                if (err) {
                    console.log("error: ", err);
                    return res.status(500).send({
                        message: err
                    });
                } else {
                    var data = {
                        data: rows
                    };
                    //  console.log(data)
                    return res.status(201).json(data);
                }
            });
        }
    });
};


exports.getSalesforcePulledData = (req, res) => {
    console.log("Get Data Table API getSalesforcePulledData...........", req.body);

    sql.query(`SELECT * FROM sdfc_leads_source_hist where for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear}`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};

// exports.InsertSalesforcePushedData = (req, res) => {
//   console.log("Insert Data API InsertSalesforcePushedData...........", req.body);
//     // Save User in the database
//     var leaddata=req.body;
//     DmaFile.insertleaddata(leaddata, (err, data) => {
//       if (err)
//         return res.status(500).send({
//           message: err.message || "Some error occurred while inserting push to salesforce lead data."
//         });
//       else return res.status(201).json(data);
//     });
//     //AddExec will come here

// };

exports.InsertSalesforcePushedData = (req, res) => {
    console.log("Get Data InsertSalesforcePushedData ...........", req.body);

    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;
    var age = '' + req.body.age;
    var income = '' + req.body.income;
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;
    let fileName = "Salesforce Leads";
    let isCheckedLeadsAll = `${req.body.isCheckedLeadsAll}`;
    var integratedLead = '' + req.body.integratedLead;

    var filterquery = '';

    if (isCheckedLeadsAll === true) {
        console.log("true..................................................................................", req.body);
        filterquery = ` and integreted_id not in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.fileMonth} and for_year = ${req.body.fileYear} and status='pushed')`;
    }

    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }
    if (req.body.income.length > 0) {
        var incomeArr = income.split(',');
        console.log(incomeArr, "incomeArr")
        const incomeFinal = incomeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and income in (${incomeFinal})`;
    }
    // if (req.body.age.length > 0) {
    //     var ageArr = age.split(',');
    //     console.log(ageArr, "ageArr")
    //     const ageFinal = ageArr.map(c => `'${c}'`).join(', ');
    //     filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) in (${ageFinal})`;
    // }

    if (req.body.age) {
        filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) BETWEEN  ${req.body.age.min} AND ${req.body.age.max}`;
    }

    if (req.body.integratedLead.length > 0) {
        var integratedLeadArr = integratedLead.split(',');
        console.log(integratedLeadArr, "integratedLeadArr")
        const integratedLeadFinal = integratedLeadArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and integreted_id in (${integratedLeadFinal})`;
    }

    var disquery = `INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) SELECT integreted_id,for_month,for_year,1 as user_id FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear} `
    // var disquery=`INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) SELECT integreted_id,for_month,for_year,1 as user_id FROM  maleads_integrated `


    if (filterquery != '') {
        disquery = disquery + filterquery;
    }
    sql.query(disquery, '', (err, result) => {
        // sql.query("INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) SELECT integreted_id,for_month,for_year,1 as user_id FROM  maleads_integrated WHERE do_not_call=0 or do_not_call is null and  disenrollment_reason!='Death' or disenrollment_reason is null and sfdc_sales_id=0 and edr_id=0 limit 10",'', (err, res) => {
        // sql.query(resquery, rows, (err, res) => {
        if (err) {
            console.log("error: ", err);
            // result(err, null);
            return;
        }


        if (os.platform().startsWith('linux')) {
            // sql.query("SELECT * FROM `lead`", function (error, data, fields) {
            sql.query(`SELECT salesforce_id as Id,city as City,address_line1 as Street,state as State,country as Country,zip_code as PostalCode,first_name as FirstName,last_name as LastName,product_family_name as Company,phone as Phone, do_not_call as DoNotCall,member_email as Email,lead_source as LeadSource,sic as SICCode__c,STR_TO_DATE(dob, '%m-%d-%Y') as Date_of_Birth__c,prospect_type as Prospect_type__c,opportunity_record_type as Opportunity_Record_Type__c,sales_channel as Sales_Channel__c,enrollment_channel as Enrollment_Channel__c,agein_type_rel as Age_In_Type__c,closed_lost_reason as Closed_Lost_Reason__c,converted as Converted__c, STR_TO_DATE(effective_date, '%m-%d-%Y') as Effective_Date__c, STR_TO_DATE(close_date, '%m-%d-%Y') as Close_Date__c,product_name as Product_Name__c, STR_TO_DATE(disenrollment_date, '%m-%d-%Y') as Disenrollment_Date__c,disenrollment_reason as Disenrollment_Reason__c,last_activity as Last_Activity__c,lead_record_type as Lead_Record_Type__c,lead_sub_type as Lead_Subtype__c,STR_TO_DATE(ma_effective_date, '%m-%d-%Y') as MA_Effective_Date__c,opp_status as Opportunity_Status__c, STR_TO_DATE(opportunity_date, '%d-%m-%Y') as Opportunity_Date__c , opportunity_id as Opportunity_Id__c,opportunity_owner as Opportunity_Owner__c,GIC_Mbr_Flag as Flag__c FROM maleaddb3.salesforce_leads where integreted_id in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.fileMonth} and for_year = ${req.body.fileYear} and status is null);`, function (error, data, fields) {
                if (error) throw error;
                const jsonData = JSON.parse(JSON.stringify(data));
                console.log("jsonData", jsonData);
                const json2csvParser = new Json2csvParser({
                    header: true
                });
                const csv = json2csvParser.parse(jsonData);
                fs.writeFile("/root/leadpoc/upload/lead.csv", csv, function (error) {
                    //  fs.writeFile("lead.csv", csv, function (error) {
                    if (error) throw error;
                    const spawn = require('child_process').spawn;
                    const ls = spawn('./export.sh', []);
                    ls.stdout.on('data', (data) => {
                        var test = data.toString().replace(/</g, '&lt;');
                        console.log(test, "Success...");
                    });
                    //  ls.stderr.on('data', (data) => {
                    //    console.log(data, "Error");
                    //  });

                    ls.on('close', (code) => {
                        console.log(code, "UPDATE salesforce push");
                        sql.query("UPDATE dmafile SET isSalesForcePush=1 WHERE userid = ? AND fileName = ? AND month = ? AND year = ?", [userId, fileName, fileMonth, fileYear], (err, res) => {
                            if (err) {
                                console.log("error: ", err);
                                // result(err, null);
                                // return;
                            }

                            sql.query("UPDATE saleaforce_pushed_data SET status='pushed' WHERE user_id = ? AND for_month = ? AND for_year = ?", [userId, fileMonth, fileYear], (err, res) => {
                                if (err) {
                                    console.log("error: ", err);
                                    // result(err, null);
                                    // return;
                                }
                                console.log("Update saleaforce_pushed_data............................................");
                                // result(null, res);
                            });
                            console.log("Update dmafile isSalesForcePush to 1 ............................................");
                            // result(null, res);
                        });

                        res.status(200).send({
                            message: "export Successfully!"
                        });
                    });
                    console.log("lead.csv successfully Created !");
                });
            });
        } else if (os.platform().startsWith('win')) {
            console.log("not implemented on Windows yet");
        }
        console.log("Success: ", res);
        // return res.status(201).json(res);
    });

};

exports.UpdatePrediction = (req, res) => {
    console.log("Inside UPdate Prediction..", req.body);
    var state = '' + req.body.state;
    var city = '' + req.body.city;
    var zipcode = '' + req.body.zipcode;
    var age = '' + req.body.age;
    var income = '' + req.body.income;
    let fileMonth = '' + `${req.body.fileMonth}`;
    let fileYear = '' + `${req.body.fileYear}`;
    let userId = '' + `${req.body.userId}`;
    let fileName = "Salesforce Leads";
    let isCheckedLeadsAll = `${req.body.isCheckedLeadsAll}`;
    var integratedLead = '' + req.body.integratedLead;
    var filterquery = '';

    if (isCheckedLeadsAll === true) {
        console.log("true..................................................................................", req.body);
        filterquery = ` and integreted_id not in (SELECT integreted_id FROM saleaforce_pushed_data WHERE for_month= ${req.body.fileMonth} and for_year = ${req.body.fileYear} and status='pushed')`;
    }
    if (req.body.state.length > 0) {
        var stateArr = state.split(',');
        console.log(stateArr, "stateArr")
        const stateFinal = stateArr.map(c => `'${c}'`).join(', ');
        filterquery = ` and state in (${stateFinal})`;
    }
    if (req.body.city.length > 0) {
        var cityArr = city.split(',');
        console.log(cityArr, "cityArr")
        const cityFinal = cityArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and city in (${cityFinal})`;
    }
    if (req.body.zipcode.length > 0) {
        var zipcodeArr = zipcode.split(',');
        console.log(zipcodeArr, "zipcodeArr")
        const zipcodeFinal = zipcodeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and zip_code in (${zipcodeFinal})`;
    }
    if (req.body.income.length > 0) {
        var incomeArr = income.split(',');
        console.log(incomeArr, "incomeArr")
        const incomeFinal = incomeArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and income in (${incomeFinal})`;
    }
    if (req.body.age) {
        filterquery = filterquery + ` and TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) BETWEEN  ${req.body.age.min} AND ${req.body.age.max}`;
    }
    if (req.body.integratedLead.length > 0) {
        var integratedLeadArr = integratedLead.split(',');
        console.log(integratedLeadArr, "integratedLeadArr")
        const integratedLeadFinal = integratedLeadArr.map(c => `'${c}'`).join(', ');
        filterquery = filterquery + ` and integreted_id in (${integratedLeadFinal})`;
    }

    var disquery = `SELECT MAD_ID as unique_id, TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) as age, for_month as month , 'M' as gender, income , education ,married, ethnicity, personicx FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear} `

    //var disquery = `INSERT INTO saleaforce_pushed_data (integreted_id,for_month,for_year,user_id) SELECT integreted_id,for_month,for_year,1 as user_id FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 and for_month  = ${req.body.fileMonth} and for_year = ${req.body.fileYear} `

    if (filterquery != '') {
        disquery = disquery + filterquery;
    }
    sql.query(disquery, '', (err, result) => {
        if (err) {
            console.log("error: ", err);
            return;
        }
        console.log("result: ", result);
        return res.status(200).json(result);
    });

};

exports.updatePredictionScore = (req, res) => {
    console.log("Inside updatePredictionScore.");
    var axios = require('axios');
    // var data = JSON.stringify([{
    //     "unique_id": 17,
    //     "age": 67,
    //     "month": "GT",
    //     "gender": "M",
    //     "income": "$175,000 - $199,999",
    //     "education": "Completed High School",
    //     "married": "Married",
    //     "ethnicity": "W",
    //     "personicx": "15M Top Wealth"
    // }, {
    //     "unique_id": 27,
    //     "age": 67,
    //     "month": "KV",
    //     "gender": "F",
    //     "income": "$45,000 - $49,999",
    //     "education": "Completed High School",
    //     "married": "Married",
    //     "ethnicity": "W",
    //     "personicx": "20S Community Minded"
    // }, {
    //     "unique_id": 47,
    //     "age": 63,
    //     "month": "CO",
    //     "gender": "F",
    //     "income": "Under $10,000",
    //     "education": "Completed High School",
    //     "married": "Single",
    //     "ethnicity": "W",
    //     "personicx": "17M Bargain Hunters"
    // }]);

    // var config = {
    //     method: 'post',
    //     url: 'http://164.52.194.25:8017/predict-sale',
    //     headers: {
    //         'Content-Type': 'application/json'
    //     },
    //     data: data
    // };

    // axios(config)
    //     .then(function (response) {
    //         console.log(JSON.stringify(response.data));
    //     })
    //     .catch(function (error) {
    //         console.log(error);
    //     });

    //-------------------------------
    var disquery = `SELECT MAD_ID as unique_id, TIMESTAMPDIFF(YEAR, STR_TO_DATE(dob,'%m-%d-%Y'), CURDATE()) as age, for_month as month , 'M' as gender, income , education ,married, ethnicity, personicx FROM  maleads_integrated WHERE (do_not_call=0 or do_not_call is null) and  (disenrollment_reason!='Death' or disenrollment_reason is null) and sfdc_sales_id=0 and edr_id=0 limit 10`;
    sql.query(disquery, '', (err, result) => {
        if (err) {
            console.log("error: ", err);
            return;
        }
        // console.log("result: ", result);

        // data = [{
        //     unique_id: 'FioEym533778235',
        //     age: 90,
        //     month: '2',
        //     gender: 'M',
        //     income: '$100,000 - $149,999',
        //     education: 'Completed High School',
        //     married: 'Married',
        //     ethnicity: 'W',
        //     personicx: '15M Top Wealth'
        // }];

        let reqdata = JSON.parse(JSON.stringify(result));
        console.log("data: ", reqdata);
        var config = {
            method: 'post',
            url: 'http://164.52.194.25:8017/predict-sale',
            headers: {
                'Content-Type': 'application/json'
            },
            data: reqdata
        };

        axios(config)
            .then(function (predctionUrlResponse) {
                // return res.status(200).json(predctionUrlResponse); 
                // let scores = JSON.stringify(predctionUrlResponse.data)
                let scores = JSON.parse(JSON.stringify(predctionUrlResponse.data));
                console.log("Prediction  Response.", scores);
                let requests = scores.map(id => {
                    return new Promise((resolve, reject) => {
                        // console.log("ids....... ", id.unique_id);
                        var requestQuery = `UPDATE maleads_integrated SET prediction_score=${id.sale_probability} WHERE MAD_ID = '${id.unique_id}';`;
                        sql.query(requestQuery, function (err, data, result) {
                            if (err) {
                                console.log("sql access error: ", err);
                                reject(err);
                            } else {
                                resolve(result);
                            }
                        });
                    })
                });
                Promise.all(requests).then((body) => {
                    return res.status(200).send({
                        message: "Updated Successfully.!"
                    });
                }).catch(err => console.log(err))

            }).catch(error => {
                console.log("Prediction  error.", error);

            });
    });
};

exports.UpdateSalesforcePushedData = (req, res) => {
    console.log("UpdateSalesforcePushedData function...........");
    var result = [];
    const leaddata = new DmaFile({
        user_id: req.body.userId,
        for_year: req.body.year,
        for_month: req.body.month,
        status: req.body.status
    });
    // Save User in the database
    DmaFile.updateleadsById(leaddata, (err, data) => {
        if (err)
            return res.status(500).send({
                message: err.message || "Some error occurred while updating the pushed salesforce lead data status."
            });
        else return res.status(201).send({
            message: "Success: Status is updated in all records Successfully..!"
        });
    });
}

exports.getSPProgressStatus = (req, res) => {
    console.log("Get Data Table API getSPProgressStatus...........", req.body);

    sql.query(`SELECT * FROM SP_Progress left join SP_Names on SP_Names.id=SP_Names_id where formonth  = ${req.body.procfileMonth} and foryear = ${req.body.procfileYear} and iscompleted=1`, function (err, data, fields) {
        // sql.query(`SELECT * FROM SP_Progress left join SP_Names on SP_Names.id=SP_Names_id where iscompleted=1`, function (err, data, fields) {

        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No data Found !"
            });
        }
        return res.status(201).json(data);

    });
};


// get table field type...
exports.getTableField = (req, res) => {
    console.log("get table fields...........", req.body.tableName);
    var dataArray

    if (req.body.tableName === "axiom") {
        dataArray = [{
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "middle_initial",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "dob",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "address_line1",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "address_line2",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "zip_code",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "phone",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "census_tract_code",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "census_block_group",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "census_block_code",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "fips_county_code",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "income",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "education",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "married",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "ethnicity",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "personicx",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "personicx_cluster",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "axciom_id",
                ORDINAL_POSITION: 21
            },
            {
                COLUMN_NAME: "MAD_ID",
                ORDINAL_POSITION: 22
            },
            {
                COLUMN_NAME: "for_month",
                ORDINAL_POSITION: 23
            },
            {
                COLUMN_NAME: "for_year",
                ORDINAL_POSITION: 24
            },
            {
                COLUMN_NAME: "isprevious",
                ORDINAL_POSITION: 25
            },
            {
                COLUMN_NAME: "ORI_MAD_ID",
                ORDINAL_POSITION: 26
            },
            {
                COLUMN_NAME: "is_mad_updated",
                ORDINAL_POSITION: 27
            },

        ];
    } else if (req.body.tableName === "clinical_data") {
        dataArray = [{
                COLUMN_NAME: "CCMS_ID",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "edr_member_id",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "LID",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "depnum",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "medicare_id",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "hicno",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "mbi",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "mid_initial",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "dob",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "address1",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "address2",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "zip_code",
                ORDINAL_POSITION: 16
            }
        ];
    } else if (req.body.tableName === "do_not_call") {

        dataArray = [{
                COLUMN_NAME: "new_LID",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "LID",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "dep_id",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "member_UD",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "birth_date",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "phone",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "address",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "zip",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "date",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "remove",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "inserted_date",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "for_month",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "for_year",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "dnc_id",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "is_inserted",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "is_updated",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "isprevious",
                ORDINAL_POSITION: 21
            },

        ];
    } else if (req.body.tableName === "edr_p65_combine_data") {
        dataArray = [{
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "street_address1_name",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "street_address2_name",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "city_name",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "state_code",
                ORDINAL_POSITION: 6
            },

            {
                COLUMN_NAME: "zip_code",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "phone_number",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "member_email",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "market_segment",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "begin_date",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "end_date",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "disenrollment_reason_code",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "account_name",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "group_id",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "subgroup_name",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "consistent_member_id",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "member_id",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "medicare_id",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "legacy_member_id",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "dob_date",
                ORDINAL_POSITION: 21
            },
            {
                COLUMN_NAME: "gender_code",
                ORDINAL_POSITION: 22
            },
            {
                COLUMN_NAME: "member_relationship_desc",
                ORDINAL_POSITION: 23
            },
            {
                COLUMN_NAME: "product_family_name",
                ORDINAL_POSITION: 24
            },
            {
                COLUMN_NAME: "legacy_benefit_package_id",
                ORDINAL_POSITION: 25
            },
            {
                COLUMN_NAME: "county",
                ORDINAL_POSITION: 26
            },
            {
                COLUMN_NAME: "renewal_month_desc",
                ORDINAL_POSITION: 27
            },
            {
                COLUMN_NAME: "inserted_date",
                ORDINAL_POSITION: 28
            },
            {
                COLUMN_NAME: "edr_p65_combine_id",
                ORDINAL_POSITION: 29
            },
            {
                COLUMN_NAME: "MAD_ID",
                ORDINAL_POSITION: 30
            },
            {
                COLUMN_NAME: "for_month",
                ORDINAL_POSITION: 31
            },
            {
                COLUMN_NAME: "for_year",
                ORDINAL_POSITION: 32
            },
            {
                COLUMN_NAME: "isprevious",
                ORDINAL_POSITION: 33
            }
        ];
    } else if (req.body.tableName === "host_hist") {
        dataArray = [{
                COLUMN_NAME: "ndw_home_plan_id",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "consistent_member_id",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "mmi_identifier",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "its_subscriber_id",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "member_date_of_birth",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "member_gender",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "member_prefix",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "member_last_name",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "member_first_name",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "member_middle_initial",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "member_suffix",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "member_primary_street_address_1",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "member_primary_street_address_2",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "member_primary_city",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "member_primary_state",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "member_primary_zip_code",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "member_primary_zip_code_4",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "member_primary_phone_number",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "member_primary_email_address",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "member_secondary_street_address_1",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "member_secondary_street_address_2",
                ORDINAL_POSITION: 21
            },
            {
                COLUMN_NAME: "member_secondary_city",
                ORDINAL_POSITION: 22
            },
            {
                COLUMN_NAME: "member_secondary_state",
                ORDINAL_POSITION: 23
            },
            {
                COLUMN_NAME: "member_secondary_zip_code",
                ORDINAL_POSITION: 24
            },
            {
                COLUMN_NAME: "member_secondary_zip_code_4",
                ORDINAL_POSITION: 25
            },
            {
                COLUMN_NAME: "reporting_account_name",
                ORDINAL_POSITION: 26
            },
            {
                COLUMN_NAME: "ndw_host_plan_id",
                ORDINAL_POSITION: 27
            },
            {
                COLUMN_NAME: "ndw_product_category_code",
                ORDINAL_POSITION: 28
            }
        ];
    } else if (req.body.tableName === "ma_recent_deceased") {
        dataArray = [{
                COLUMN_NAME: "firstname",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "lastname",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "dateofbirth",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "age",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "gender",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "discharge_date",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "discharge_disposition",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "consistent_id",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "LID",
                ORDINAL_POSITION: 9
            }
        ];
    } else if (req.body.tableName === "mailings") {
        dataArray = [{
                COLUMN_NAME: "date",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "mailing_name",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "mailing_number",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "member_id",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "ccid",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "name",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "address_line_1",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "address_line_2",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "zip",
                ORDINAL_POSITION: 11
            }
        ];
    } else if (req.body.tableName === "marketo") {
        dataArray = [{
                COLUMN_NAME: "date_sent",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "email_name",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "segment",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "full_name",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "email_address",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "member_id",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "opened",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "clicked",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "zipcode",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "inserted_date",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "for_month",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "for_year",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "marketo_id",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "isprevious",
                ORDINAL_POSITION: 14
            }
        ];
    } else if (req.body.tableName === "national_alliance_hist") {
        dataArray = [{
                COLUMN_NAME: "mbr_no",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "pat_lname",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "pat_fname",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "subscr_id",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "subs_address1",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "subs_address2",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "subs_address3",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "subs_city",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "subs_state",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "subs_zip_cd",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "pat_dob",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "pat_sex",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "pat_sex_rel",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "group_prfx",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "group_base",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "group_sufx",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "lob_cd",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "over_age_cd",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "ce_alternate_id",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "ttb_id_number",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "pat_id",
                ORDINAL_POSITION: 21
            },
            {
                COLUMN_NAME: "eol_indicator",
                ORDINAL_POSITION: 22
            }
        ];
    } else if (req.body.tableName === "prodcopy") {
        dataArray = [{
                COLUMN_NAME: "LID",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "dependent_id",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "email_address",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "lastlogindate",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "datecreated",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "dateupdated",
                ORDINAL_POSITION: 6
            }
        ];
    } else if (req.body.tableName === "sdfc_leads_source_hist") {
        dataArray = [{
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "address1",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "address2",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "zip_code",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "dob",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "email_address",
                ORDINAL_POSITION: 9
            },
            {
                COLUMN_NAME: "phone_number",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "account_id",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "prospect_type",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "territory",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "lead_owner",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "bcbs_reference_id",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "lead_number",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "ma_effective_date",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "lead_source",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "status",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "lead_sub_type",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "created_date",
                ORDINAL_POSITION: 21
            },

            {
                COLUMN_NAME: "do_not_call",
                ORDINAL_POSITION: 22
            },
            {
                COLUMN_NAME: "unread_by_owner",
                ORDINAL_POSITION: 23
            },
            {
                COLUMN_NAME: "converted",
                ORDINAL_POSITION: 24
            },
            {
                COLUMN_NAME: "opportunity_date",
                ORDINAL_POSITION: 25
            },
            {
                COLUMN_NAME: "opportunity_owner",
                ORDINAL_POSITION: 26
            },
            {
                COLUMN_NAME: "stage",
                ORDINAL_POSITION: 27
            },
            {
                COLUMN_NAME: "closed_lost_reason",
                ORDINAL_POSITION: 28
            },
            {
                COLUMN_NAME: "opp_status",
                ORDINAL_POSITION: 29
            },
            {
                COLUMN_NAME: "age_in_type",
                ORDINAL_POSITION: 30
            },
            {
                COLUMN_NAME: "current_carrier",
                ORDINAL_POSITION: 31
            },
            {
                COLUMN_NAME: "disenrollment_date",
                ORDINAL_POSITION: 32
            },
            {
                COLUMN_NAME: "disenrollment_reason",
                ORDINAL_POSITION: 33
            },
            {
                COLUMN_NAME: "sic",
                ORDINAL_POSITION: 34
            },
            {
                COLUMN_NAME: "last_activity",
                ORDINAL_POSITION: 35
            },
            {
                COLUMN_NAME: "last_modified",
                ORDINAL_POSITION: 36
            },
            {
                COLUMN_NAME: "lead_id",
                ORDINAL_POSITION: 37
            },
            {
                COLUMN_NAME: "sfdc_id",
                ORDINAL_POSITION: 38
            },
            {
                COLUMN_NAME: "opportunity_id",
                ORDINAL_POSITION: 39
            },
            {
                COLUMN_NAME: "lead_record_type",
                ORDINAL_POSITION: 40
            },
            {
                COLUMN_NAME: "opportunity_record_type",
                ORDINAL_POSITION: 41
            }

        ];
    } else if (req.body.tableName === "sdfc_sales_source_hist") {
        dataArray = [{
                COLUMN_NAME: "first_name",
                ORDINAL_POSITION: 1
            },
            {
                COLUMN_NAME: "last_name",
                ORDINAL_POSITION: 2
            },
            {
                COLUMN_NAME: "account_name",
                ORDINAL_POSITION: 3
            },
            {
                COLUMN_NAME: "address_line1",
                ORDINAL_POSITION: 4
            },
            {
                COLUMN_NAME: "address_line2",
                ORDINAL_POSITION: 5
            },
            {
                COLUMN_NAME: "city",
                ORDINAL_POSITION: 6
            },
            {
                COLUMN_NAME: "state",
                ORDINAL_POSITION: 7
            },
            {
                COLUMN_NAME: "zip_code",
                ORDINAL_POSITION: 8
            },
            {
                COLUMN_NAME: "birthdate",
                ORDINAL_POSITION: 9
            },

            {
                COLUMN_NAME: "email",
                ORDINAL_POSITION: 10
            },
            {
                COLUMN_NAME: "phone",
                ORDINAL_POSITION: 11
            },
            {
                COLUMN_NAME: "prospect_type",
                ORDINAL_POSITION: 12
            },
            {
                COLUMN_NAME: "territory",
                ORDINAL_POSITION: 13
            },
            {
                COLUMN_NAME: "bcbs_reference_id",
                ORDINAL_POSITION: 14
            },
            {
                COLUMN_NAME: "stage",
                ORDINAL_POSITION: 15
            },
            {
                COLUMN_NAME: "opp_status",
                ORDINAL_POSITION: 16
            },
            {
                COLUMN_NAME: "closed_lost_reason",
                ORDINAL_POSITION: 17
            },
            {
                COLUMN_NAME: "age_in_type",
                ORDINAL_POSITION: 18
            },
            {
                COLUMN_NAME: "disenrollment_date",
                ORDINAL_POSITION: 19
            },
            {
                COLUMN_NAME: "disenrollment_reason",
                ORDINAL_POSITION: 20
            },
            {
                COLUMN_NAME: "opportunity_owner",
                ORDINAL_POSITION: 21
            },
            {
                COLUMN_NAME: "last_activity",
                ORDINAL_POSITION: 22
            },
            {
                COLUMN_NAME: "last_modified_date",
                ORDINAL_POSITION: 23
            },
            {
                COLUMN_NAME: "product_name",
                ORDINAL_POSITION: 24
            },
            {
                COLUMN_NAME: "close_date",
                ORDINAL_POSITION: 25
            },
            {
                COLUMN_NAME: "effective_date",
                ORDINAL_POSITION: 26
            },
            {
                COLUMN_NAME: "enrollment_channel",
                ORDINAL_POSITION: 27
            },
            {
                COLUMN_NAME: "sep_type",
                ORDINAL_POSITION: 28
            },
            {
                COLUMN_NAME: "medicare_id",
                ORDINAL_POSITION: 29
            },
            {
                COLUMN_NAME: "enrollment_method",
                ORDINAL_POSITION: 30
            },
            {
                COLUMN_NAME: "sales_channel",
                ORDINAL_POSITION: 31
            },
            {
                COLUMN_NAME: "county",
                ORDINAL_POSITION: 32
            },

            {
                COLUMN_NAME: "enrollment_source",
                ORDINAL_POSITION: 33
            },
            {
                COLUMN_NAME: "lead_source",
                ORDINAL_POSITION: 34
            },
            {
                COLUMN_NAME: "opportunity_id",
                ORDINAL_POSITION: 35
            },
            {
                COLUMN_NAME: "account_id",
                ORDINAL_POSITION: 36
            },
            {
                COLUMN_NAME: "opportunity_record_type",
                ORDINAL_POSITION: 37
            },
            {
                COLUMN_NAME: "account_record_type",
                ORDINAL_POSITION: 38
            },
            {
                COLUMN_NAME: "current_blue_cross_id",
                ORDINAL_POSITION: 39
            },
            {
                COLUMN_NAME: "do_not_call",
                ORDINAL_POSITION: 40
            },
            {
                COLUMN_NAME: "deceased",
                ORDINAL_POSITION: 41
            },
            {
                COLUMN_NAME: "member_type",
                ORDINAL_POSITION: 42
            },
            {
                COLUMN_NAME: "sales_date",
                ORDINAL_POSITION: 43
            },
            {
                COLUMN_NAME: "sfdc_id",
                ORDINAL_POSITION: 44
            },
            {
                COLUMN_NAME: "lead_id",
                ORDINAL_POSITION: 45
            },
            {
                COLUMN_NAME: "inserted_date",
                ORDINAL_POSITION: 46
            },
            {
                COLUMN_NAME: "for_month",
                ORDINAL_POSITION: 47
            },
            {
                COLUMN_NAME: "for_year",
                ORDINAL_POSITION: 48
            },
            {
                COLUMN_NAME: "sfdc_sales_source_id",
                ORDINAL_POSITION: 49
            },
            {
                COLUMN_NAME: "MAD_ID",
                ORDINAL_POSITION: 50
            },
            {
                COLUMN_NAME: "isprevious",
                ORDINAL_POSITION: 51
            },
            {
                COLUMN_NAME: "ORI_MAD_ID",
                ORDINAL_POSITION: 52
            },
            {
                COLUMN_NAME: "is_mad_updated",
                ORDINAL_POSITION: 53
            }
        ];
    }

    sql.query(`select COLUMN_NAME, ORDINAL_POSITION from information_schema.columns where TABLE_SCHEMA = 'maleaddb3' AND table_name = '${req.body.tableName}'`, (err, data) => {
        if (err) {
            console.log("error: ", err);
            return res.status(500).send({
                message: err
            });
        }
        if (data.length <= 0) {
            return res.status(200).send({
                message: "No Table Found !"
            });
        }
        //  var tableResult = (buildTable(data));
        //  console.log(tableResult, "table Result");
        //  return res.status(201).send(tableResult);
        return res.status(201).json(dataArray);
        //  return res.status(201).send(data);
    });
};