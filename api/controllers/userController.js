
const User = require("../models/userModel");

exports.debugUserAPI = (req, res) => {
  res.status(200).send({
    message: "Successfully Tested.."
  });
};



// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a User
  const user = new User({
    email: req.body.email,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    apassword: req.body.apassword
  });

  // Save User in the database
  User.create(user, (err, data) => {
    if (err)
      if (err.kind === "already_exist") {
        res.status(409).send({
          message: " The username already exists. Please use a different username"
        });
      } else {
        return res.status(500).send({
          message: err.message || "Some error occurred while creating the User."
        });
      }
    else return res.status(201).json(data);
  });
};

// Create and Save a new User
exports.signIn = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a User Object
  const user = new User({
    email: req.body.email,
    apassword: req.body.apassword
  });

  // User SignIn
  User.signIn(user, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "Invalid credentials"
        });
      } else {
        res.status(500).send({
          message: "Some error occurred while SignIn user" + req.body.email
        });
      }
    } else return res.status(201).json(data);
  });
};


// Retrieve all users from the database.
exports.findAll = (req, res) => {
  User.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving users."
      });
    else res.send(data);
  });
};

// Find a single User with a customerId
exports.findOne = (req, res) => {
  User.findById(req.params.customerId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with id ${req.params.customerId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving User with id " + req.params.customerId
        });
      }
    } else res.send(data);
  });
};

// Update a User identified by the customerId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  console.log(req.body);
  User.updateById(
    req.params.customerId,
    new User(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found User with id ${req.params.customerId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating User with id " + req.params.customerId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a User with the specified customerId in the request
exports.delete = (req, res) => {
  User.remove(req.params.customerId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with id ${req.params.customerId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete User with id " + req.params.customerId
        });
      }
    } else res.send({
      message: `User was deleted successfully!`
    });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  User.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while removing all customers."
      });
    else res.send({
      message: `All users were deleted successfully!`
    });
  });
};
