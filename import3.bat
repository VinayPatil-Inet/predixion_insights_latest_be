@echo on
SET MYSQLUSER=root 
SET PASSWORD=rootadmin
SET DB=maleaddb3

SET FILENAME=%1
SET TABLENAME=%2
SET MONTH=%3
SET YEAR=%4

mysql -u %MYSQLUSER% -p%PASSWORD% --database=%DB% -AN --local-infile=1 -e "LOAD DATA LOCAL INFILE '%FILENAME%' INTO TABLE %TABLENAME% FIELDS TERMINATED BY ','  ENCLOSED BY '\"'LINES TERMINATED BY '\n' IGNORE 1 ROWS"

mysql -u%MYSQLUSER% -p%PASSWORD% --database=%DB% -e "UPDATE %TABLENAME% SET for_month='%MONTH%', for_year='%YEAR%' WHERE isprevious=0;"

ECHO '%TABLENAME%'.csv Successfully Imported. 
